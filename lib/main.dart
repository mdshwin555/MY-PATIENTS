import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sizer/sizer.dart';
import 'View/Screens/CuponInput.dart';
import 'View/Screens/Home.dart';
import 'notification_manager/notification_manager.dart';
import 'package:timezone/data/latest.dart' as tz;
import 'package:firebase_core/firebase_core.dart';

SharedPreferences? sharedPref;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  sharedPref = await SharedPreferences.getInstance();
  NotificationService().initNotification();
  tz.initializeTimeZones();
  await Firebase.initializeApp();
  runApp(MyApp());

  ///////////////////////////////
  //sharedPref?.clear();
  ///////////////////////////////
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Sizer(
      builder: (BuildContext context, Orientation orientation,
          DeviceType deviceType) {
        return GetMaterialApp(
          theme: ThemeData(
            //primaryColor: Color(0xff256D85),
            textTheme: TextTheme(
              button: TextStyle(
                color: Color(0xff256D85), // Set the text color to red
                fontWeight: FontWeight.bold, // Make the text bold
                fontSize: 15,
                fontFamily: 'Exo',
              ),
            ),
          ),
          textDirection: TextDirection.rtl,
          debugShowCheckedModeBanner: false,
          home: sharedPref?.getBool("isLoged")==true?Home():InputCupons(),
        );
      },
    );
  }
}
