import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_iconly/flutter_iconly.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:sizer/sizer.dart';
import '../../Controllers/CountdownController.dart';
import '../../Controllers/RemaindController.dart';
import '../../notification_manager/notification_manager.dart';
import 'Home.dart';

class RemainderList extends StatelessWidget {
  final MyController countdownController = MyController();
  ScrollController _scrollController = ScrollController();
  final FlutterLocalNotificationsPlugin notificationsPlugin =
      FlutterLocalNotificationsPlugin();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        backgroundColor: Color(0xff256D85),
        title: Text(
          'المواعيد المحجوزة',
          style: TextStyle(
            fontSize: 11.sp,
            fontFamily: 'Exo',
            fontWeight: FontWeight.bold,
            letterSpacing: 0.1.w,
            color: Colors.white,
          ),
        ),
        leading: IconButton(
          onPressed: () {
            Get.to(Home());
          },
          icon: Icon(IconlyLight.arrowRight2),
        ),
        //centerTitle: true,
      ),
      body: GetX<RemaindController>(
        init: RemaindController(),
        builder: (controller) {
          final remaind = controller.remaind;
          return controller.remaind.length == 0
              ? Center(
                  child: Text(
                    'لا يوجد مواعيد لديك ',
                    style: TextStyle(
                      fontSize: 13.sp,
                      fontFamily: 'Exo',
                      fontWeight: FontWeight.bold,
                      letterSpacing: 0.1.w,
                      color: Colors.black54,
                    ),
                  ),
                )
              : Column(
                  children: [
                    SizedBox(
                      height: 1.h,
                    ),
                    Text(
                      remaind.length == 1
                          ? 'لديك موعد واحد'
                          : remaind.length == 2
                              ? 'لديك موعدين '
                              : remaind.length >= 3 && remaind.length <= 10
                                  ? 'لديك ${remaind.length} مواعيد'
                                  : 'لديك ${remaind.length} موعد',
                      style: TextStyle(
                        fontSize: 11.sp,
                        fontFamily: 'Exo',
                        //fontWeight: FontWeight.bold,
                      ),
                    ),
                    Flexible(
                      child: Container(
                        height: 100.h,
                        width: 100.w,
                        margin: EdgeInsets.only(
                          top: 2.h,
                        ),
                        child: ListView.separated(
                          //reverse: true,
                          itemCount: remaind.length,
                          itemBuilder: (context, index) {
                            final field = remaind[index];

                            return Stack(
                              children: [
                                Container(
                                  margin: EdgeInsets.only(
                                      left: 3.w, right: 3.w, bottom: 3.h),
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(10.sp),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.grey.withOpacity(0.7),
                                        blurRadius: 6.0,
                                        offset: Offset(0, 2),
                                      ),
                                    ],
                                  ),
                                  child: Stack(
                                    children: [
                                      ListTile(
                                        title: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              field.name,
                                              style: TextStyle(
                                                fontSize: 12.sp,
                                                fontFamily: 'Exo',
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                          ],
                                        ),
                                        subtitle: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            SizedBox(
                                              height: 1.h,
                                            ),
                                            Row(
                                              children: [
                                                CircleAvatar(
                                                  radius: 11.sp,
                                                  child: Image.asset(
                                                    'assets/images/surgery-tools.png',
                                                    fit: BoxFit.cover,
                                                  ),
                                                ),
                                                SizedBox(
                                                  width: 2.w,
                                                ),
                                                Text(
                                                  field.operatine,
                                                  style: TextStyle(
                                                    fontSize: 11.sp,
                                                    fontFamily: 'Exo',
                                                    color: Colors.grey[600],
                                                  ),
                                                ),
                                              ],
                                            ),
                                            SizedBox(
                                              height: 1.h,
                                            ),
                                            Row(
                                              children: [
                                                CircleAvatar(
                                                  radius: 11.sp,
                                                  child: Image.asset(
                                                    'assets/images/google-maps.png',
                                                    fit: BoxFit.cover,
                                                  ),
                                                ),
                                                SizedBox(
                                                  width: 2.w,
                                                ),
                                                Text(
                                                  field.hospital,
                                                  style: TextStyle(
                                                    fontSize: 11.sp,
                                                    fontFamily: 'Exo',
                                                    color: Colors.grey[600],
                                                  ),
                                                ),
                                              ],
                                            ),
                                            SizedBox(
                                              height: 1.h,
                                            ),
                                            Row(
                                              children: [
                                                CircleAvatar(
                                                  radius: 11.sp,
                                                  child: Image.asset(
                                                    'assets/images/calendar.png',
                                                    fit: BoxFit.cover,
                                                  ),
                                                ),
                                                SizedBox(
                                                  width: 2.w,
                                                ),
                                                Text(
                                                  DateFormat('yyyy/MM/dd')
                                                      .format(DateTime.parse(
                                                          field.date)),
                                                  style: TextStyle(
                                                    fontSize: 11.sp,
                                                    fontFamily: 'Exo',
                                                    color: Colors.grey[600],
                                                  ),
                                                ),
                                              ],
                                            ),
                                            SizedBox(
                                              height: 1.h,
                                            ),
                                            Row(
                                              children: [
                                                CircleAvatar(
                                                  radius: 11.sp,
                                                  backgroundColor: Colors.transparent,
                                                  child: Image.asset(
                                                    'assets/images/telephone-call.png',
                                                    fit: BoxFit.cover,
                                                  ),
                                                ),
                                                SizedBox(
                                                  width: 2.w,
                                                ),
                                                Text(
                                                  field.phone,
                                                  style: TextStyle(
                                                    fontSize: 11.sp,
                                                    fontFamily: 'Exo',
                                                    color: Colors.grey[600],
                                                  ),
                                                ),
                                              ],
                                            ),
                                            SizedBox(
                                              height: 1.h,
                                            ),
                                          ],
                                        ),
                                      ),
                                      Positioned(
                                        left: -40,
                                        top: 20,
                                        child: Transform.rotate(
                                          angle: -45 * 3.14 / 180,
                                          child: Container(
                                            width: 50.w,
                                            height: 4.h,
                                            decoration: BoxDecoration(
                                              color: Colors.green[300],
                                            ),
                                            child: Transform.translate(
                                              offset: Offset(-14.w, 0.9.h),
                                              child: StatefulBuilder(
                                                builder: (BuildContext context, void Function(void Function()) setState) {
                                                  DateTime endDate = DateTime.parse('${field.date} ${field.time}');
                                                  Duration remainingDuration = endDate.difference(DateTime.now());
                                                  String remainingTime = ' ${remainingDuration.inDays} يوم ${remainingDuration.inHours % 24} ساعة ${remainingDuration.inMinutes % 60} دقيقة ';
                                                  if (remainingTime == ' 0 يوم 0 ساعة 0 دقيقة ') {
                                                    Get.find<RemaindController>().deleteField(index);
                                                    return SizedBox.shrink();
                                                  }
                                                  return Text(
                                                    remainingTime,
                                                    style: TextStyle(
                                                      fontSize: 8.5.sp,
                                                      fontFamily: 'Exo',
                                                      color: Colors.white,
                                                    ),
                                                  );
                                                },
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                      Positioned(
                                        left: -1.w,
                                        top: -0.5.h,
                                        child: IconButton(
                                          onPressed: () {
                                            showDialog(
                                              context: context,
                                              builder: (BuildContext context) {
                                                return Dialog(
                                                  shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            16),
                                                  ),
                                                  child: Padding(
                                                    padding:
                                                        const EdgeInsets.all(
                                                            24),
                                                    child: Column(
                                                      mainAxisSize:
                                                          MainAxisSize.min,
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Row(
                                                          children: [
                                                            CircleAvatar(
                                                              radius: 15.sp,
                                                              backgroundColor:
                                                                  Colors
                                                                      .red[400],
                                                              child: Icon(
                                                                CupertinoIcons
                                                                    .delete,
                                                                color: Colors
                                                                    .white,
                                                              ),
                                                            ),
                                                            SizedBox(
                                                              width: 2.w,
                                                            ),
                                                            Text(
                                                              'حذف الموعد',
                                                              style: TextStyle(
                                                                fontSize: 12.sp,
                                                                fontFamily:
                                                                    'Exo',
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                                letterSpacing:
                                                                    0.1.w,
                                                                color: Colors
                                                                    .black,
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                        SizedBox(height: 16),
                                                        Text(
                                                          'هل أنت متأكد من رغبتك في حذف هذا الموعد ؟',
                                                          style: TextStyle(
                                                            fontSize: 10.sp,
                                                            fontFamily: 'Exo',
                                                            color:
                                                                Colors.black54,
                                                          ),
                                                        ),
                                                        SizedBox(height: 24),
                                                        Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .end,
                                                          children: [
                                                            Expanded(
                                                              child:
                                                                  MaterialButton(
                                                                child: Text(
                                                                  'لا',
                                                                  style:
                                                                      TextStyle(
                                                                    fontSize:
                                                                        9.sp,
                                                                    fontFamily:
                                                                        'Exo',
                                                                    //fontWeight: FontWeight.bold,
                                                                    //letterSpacing: 0.1.w,
                                                                    color: Colors
                                                                        .black,
                                                                  ),
                                                                ),
                                                                color: Colors
                                                                    .white,
                                                                shape:
                                                                    RoundedRectangleBorder(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              8.0),
                                                                  side:
                                                                      BorderSide(
                                                                    color: Colors
                                                                        .black,
                                                                    width:
                                                                        0.2.w,
                                                                  ),
                                                                ),
                                                                onPressed: () {
                                                                  Navigator.of(
                                                                          context)
                                                                      .pop();
                                                                },
                                                              ),
                                                            ),
                                                            SizedBox(width: 16),
                                                            Expanded(
                                                              child:
                                                                  MaterialButton(
                                                                child: Text(
                                                                  'نعم بالتأكيد',
                                                                  style:
                                                                      TextStyle(
                                                                    fontSize:
                                                                        9.sp,
                                                                    fontFamily:
                                                                        'Exo',
                                                                    //fontWeight: FontWeight.bold,
                                                                    //letterSpacing: 0.1.w,
                                                                    color: Colors
                                                                        .white,
                                                                  ),
                                                                ),
                                                                color: Colors
                                                                    .red[400],
                                                                shape:
                                                                    RoundedRectangleBorder(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              8.0),
                                                                ),
                                                                onPressed: () {
                                                                  Navigator.of(
                                                                          context)
                                                                      .pop();
                                                                  // NotificationService()
                                                                  //     .stopNotification();
                                                                  Get.find<
                                                                          RemaindController>()
                                                                      .deleteField(
                                                                          index);
                                                                },
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                );
                                              },
                                            );
                                          },
                                          icon: Icon(
                                            CupertinoIcons.delete,
                                            color: Colors.red[400],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            );
                          },
                          separatorBuilder: (BuildContext context, int index) {
                            return SizedBox(
                              height: 0.h,
                            );
                          },
                        ),
                      ),
                    ),
                  ],
                );
        },
      ),
    );
  }
}
