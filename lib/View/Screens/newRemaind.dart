import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_iconly/flutter_iconly.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:sizer/sizer.dart';

import '../../Controllers/CountdownController.dart';
import '../../Controllers/RemaindController.dart';
import '../../main.dart';
import '../../notification_manager/notification_manager.dart';
import '../Widgets/CustomField.dart';
import 'RemainderList.dart';


class NewRemaind extends StatelessWidget {
  TextEditingController _nameController = TextEditingController();
  TextEditingController _dateController = TextEditingController();
  TextEditingController _operatineController = TextEditingController();
  TextEditingController _hospitalController = TextEditingController();
  TextEditingController _phoneController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController controller = TextEditingController();
  TextEditingController RemaindTime = TextEditingController();
  List<XFile>? images;
  final MyController countdownController = MyController();

  @override
  Widget build(BuildContext context) {

    String? path = sharedPref?.getString("path");
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xff256D85),
        title: Text(
          'حجز موعد',
          style: TextStyle(
            fontSize: 11.sp,
            fontFamily: 'Exo',
            fontWeight: FontWeight.bold,
            color: Colors.white,
          ),
        ),
        leading: IconButton(
          onPressed: () {
            Get.back();
          },
          icon: Icon(IconlyLight.arrowRight2),
        ),
        actions: [
          IconButton(
            onPressed: () {
              Get.to(RemainderList());
              print(DateTime.now());
            },
            icon: Icon(IconlyLight.calendar),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.only(
            top: 2.h,
            left: 5.w,
            right: 5.w,
            bottom: MediaQuery.of(context).viewInsets.bottom + 5.h,
          ),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'اسم المريض',
                  style: TextStyle(
                    fontSize: 11.sp,
                    fontFamily: 'Exo',
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                  ),
                ),
                CustomFields(
                  isTime:false,
                  label: 'أحمد',
                  isDate: false,
                  isPhone: false,
                  icon: Padding(
                    padding: EdgeInsets.only(
                      right: 1.4.w,
                      left: 1.4.w,
                    ),
                    child: CircleAvatar(
                      backgroundColor: Colors.transparent,
                      radius: 5.sp,
                      child: Image.asset(
                        'assets/images/profile.png',
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  controller: _nameController,
                  isNotes: false,
                  isPhoto: false,
                  validator: (String? value) {
                    if (value!.isEmpty) {
                      return 'يرجى إدخال الاسم';
                    }
                  },
                ),
                Text(
                  'العملية',
                  style: TextStyle(
                    fontSize: 11.sp,
                    fontFamily: 'Exo',
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                  ),
                ),
                Row(
                  children: [
                    Expanded(
                      flex: 4,
                      child: CustomFields(
                        isTime:false,
                        label: ' مرارة',
                        isDate: false,
                        isPhone: false,
                        icon: Padding(
                          padding: EdgeInsets.only(
                            right: 1.5.w,
                            left: 1.5.w,
                          ),
                          child: CircleAvatar(
                            radius: 5.sp,
                            child: Image.asset(
                              'assets/images/surgery-tools.png',
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        controller: _operatineController,
                        isNotes: false,
                        isPhoto: false,
                        validator: (String? value) {
                          if (value!.isEmpty) {
                            return 'يرجى إدخال العملية';
                          }
                        },
                      ),
                    ),
                  ],
                ),
                Text(
                  'تاريخ العملية',
                  style: TextStyle(
                    fontSize: 11.sp,
                    fontFamily: 'Exo',
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                  ),
                ),
                CustomFields(
                  isTime:false,
                  label: '${DateFormat('yyyy/MM/dd').format(DateTime.now())}',
                  isDate: true,
                  isPhone: false,
                  icon: Padding(
                    padding: EdgeInsets.only(
                      right: 1.4.w,
                      left: 1.4.w,
                    ),
                    child: CircleAvatar(
                      backgroundColor: Colors.transparent,
                      radius: 5.sp,
                      child: Image.asset(
                        'assets/images/calendar.png',
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  controller: _dateController,
                  isNotes: false,
                  isPhoto: false,
                  validator: (String? value) {
                    if (value!.isEmpty) {
                      return 'يرجى إدخال التاريخ';
                    }
                  },
                ),
                Text(
                  'وقت التذكير ',
                  style: TextStyle(
                    fontSize: 11.sp,
                    fontFamily: 'Exo',
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                  ),
                ),
                CustomFields(
                  isTime:true,
                  label: '${DateFormat('HH:mm').format(DateTime.now())}',
                  isDate: false,
                  isPhone: false,
                  icon: Padding(
                    padding: EdgeInsets.only(
                      right: 1.4.w,
                      left: 1.4.w,
                    ),
                    child: CircleAvatar(
                      backgroundColor: Colors.transparent,
                      radius: 5.sp,
                      child: Image.asset(
                        'assets/images/stopwatch.png',
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  controller: RemaindTime,
                  isNotes: false,
                  isPhoto: false,
                  validator: (String? value) {
                    if (value!.isEmpty) {
                      return 'يرجى إدخال الوقت';
                    }
                  },
                ),
                Text(
                  'المستشفى',
                  style: TextStyle(
                    fontSize: 11.sp,
                    fontFamily: 'Exo',
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                  ),
                ),
                Row(
                  children: [
                    Expanded(
                      flex: 4,
                      child: CustomFields(
                        isTime:false,
                        label: 'مشفى المواساة',
                        isDate: false,
                        isPhone: false,
                        icon: Padding(
                          padding: EdgeInsets.only(
                            right: 1.5.w,
                            left: 1.5.w,
                          ),
                          child: CircleAvatar(
                            radius: 5.sp,
                            child: Image.asset(
                              'assets/images/hospital.png',
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        controller: _hospitalController,
                        isNotes: false,
                        isPhoto: false,
                        validator: (String? value) {
                          if (value!.isEmpty) {
                            return 'يرجى إدخال المشفى';
                          }
                        },
                      ),
                    ),
                  ],
                ),
                Text(
                  'موبايل المريض',
                  style: TextStyle(
                    fontSize: 11.sp,
                    fontFamily: 'Exo',
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                  ),
                ),
                CustomFields(
                  isTime:false,
                  label: '875 198 938 963+',
                  isDate: false,
                  isPhone: true,
                  icon: Padding(
                    padding: EdgeInsets.only(
                      right: 1.4.w,
                      left: 1.4.w,
                    ),
                    child: CircleAvatar(
                      radius: 5.sp,
                      backgroundColor: Colors.transparent,
                      child: Image.asset(
                        'assets/images/telephone-call.png',
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  controller: _phoneController,
                  isNotes: false,
                  isPhoto: false,
                  validator: (String? value) {
                    if (value!.isEmpty) {
                      return 'يرجى إدخال موبايل المريض';
                    }
                  },
                ),
                SizedBox(
                  height: 3.h,
                ),
                MaterialButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(14.sp),
                  ),
                  minWidth: 90.w,
                  height: 7.5.h,
                  color: Color(0xff256D85),
                  onPressed: () async {
                    final name = _nameController.text;
                    final operatine = _operatineController.text;
                    final date = _dateController.text;
                    final hospital = _hospitalController.text;
                    final phonee = _phoneController.text;
                    final time = RemaindTime.text;
                    final remaind = Remaind(
                      name: name,
                      operatine: operatine,
                      date: date,
                      hospital: hospital,
                      phone: phonee,
                      time: time,
                    );
                    if (!_formKey.currentState!.validate()) {
                    } else {
                      DateTime endDate =
                      DateTime.parse('${date} ${time}');
                      countdownController.startCountdown(endDate);
                      DateTime.parse(date);
                      TimeOfDay remaindTime = TimeOfDay.fromDateTime(DateTime.parse('${date} ${RemaindTime.text}'));
                      debugPrint('Notification Scheduled for ${date} ${RemaindTime.text}');
                      NotificationService().scheduleNotification(
                        title: 'أتمنى أن تكون بخير 🤩',
                        body: 'أردت فقط تذكيرك بأن لديك موعد مع المريض $name',
                        scheduledNotificationDateTime: DateTime(
                          DateTime.parse(date).year,
                          DateTime.parse(date).month,
                          DateTime.parse(date).day,
                          remaindTime.hour,
                          remaindTime.minute,
                        ),
                      );

                      //print('${date} ${time}');
                      Get.put(RemaindController());
                      Get.find<RemaindController>().aaddField(remaind);
                      Get.to(RemainderList());
                    }
                  },
                  child: Text(
                    'إضافة الموعد',
                    style: TextStyle(
                      fontSize: 15.sp,
                      fontFamily: 'Exo',
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
