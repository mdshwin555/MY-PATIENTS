import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:sizer/sizer.dart';
import '../../Controllers/FieldsController.dart';
import '../../main.dart';
import '../Widgets/CustomField.dart';
import 'package:flutter_iconly/flutter_iconly.dart';

import 'OperatedPatients.dart';

class new_patients extends StatelessWidget {
  TextEditingController _nameController = TextEditingController();
  TextEditingController _dateController = TextEditingController();
  TextEditingController _operatineController = TextEditingController();
  TextEditingController _hospitalController = TextEditingController();
  TextEditingController _notesController = TextEditingController();
  TextEditingController _histopathologyyController = TextEditingController();
  TextEditingController _phoneController = TextEditingController();
  TextEditingController _imagesController = TextEditingController();

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String selectedHospital = 'لم يتم الإختيار';
  String selectedCity = 'لم يتم الإختيار';
  String selectedOperationKind = 'لم يتم الإختيار';
  TextEditingController controller = TextEditingController();
  List<XFile>? images;

  @override
  Widget build(BuildContext context) {
    String? path = sharedPref?.getString("path");
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xff256D85),
        title: Text(
          'مريض جديد ',
          style: TextStyle(
            fontSize: 11.sp,
            fontFamily: 'Exo',
            fontWeight: FontWeight.bold,
            color: Colors.white,
          ),
        ),
        leading: IconButton(
          onPressed: () {
            Get.back();
          },
          icon: Icon(IconlyLight.arrowRight2),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.only(
            top: 2.h,
            left: 5.w,
            right: 5.w,
            bottom: MediaQuery.of(context).viewInsets.bottom + 5.h,
          ),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'اسم المريض',
                  style: TextStyle(
                    fontSize: 11.sp,
                    fontFamily: 'Exo',
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                  ),
                ),
                CustomFields(
                  isTime:false,
                  label: 'أحمد',
                  isDate: false,
                  isPhone: false,
                  icon: Padding(
                    padding: EdgeInsets.only(
                      right: 1.4.w,
                      left: 1.4.w,
                    ),
                    child: CircleAvatar(
                      backgroundColor: Colors.transparent,
                      radius: 5.sp,
                      child: Image.asset(
                        'assets/images/profile.png',
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  controller: _nameController,
                  isNotes: false,
                  isPhoto: false,
                  validator: (String? value) {
                    if (value!.isEmpty) {
                      return 'يرجى إدخال الاسم';
                    }
                  },
                ),
                Text(
                  'العملية',
                  style: TextStyle(
                    fontSize: 11.sp,
                    fontFamily: 'Exo',
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                  ),
                ),
                Row(
                  children: [
                    Expanded(
                      flex: 4,
                      child: CustomFields(
                        isTime:false,
                        label: 'مرارة ',
                        isDate: false,
                        isPhone: false,
                        icon: Padding(
                          padding: EdgeInsets.only(
                            right: 1.5.w,
                            left: 1.5.w,
                          ),
                          child: CircleAvatar(
                            radius: 5.sp,
                            child: Image.asset(
                              'assets/images/surgery-tools.png',
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        controller: _operatineController,
                        isNotes: false,
                        isPhoto: false,
                        validator: (String? value) {
                          if (value!.isEmpty) {
                            return 'يرجى إدخال العملية';
                          }
                        },
                      ),
                    ),
                    Expanded(
                      child: Container(
                        height: 8.8.h,
                        margin: EdgeInsets.only(right: 2.w, top: 1.h),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15),
                          color: Color(0xffEAEAEA),
                        ),
                        child: PopupMenuButton(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15.sp),
                          ),
                          itemBuilder: (BuildContext context) {
                            return <PopupMenuEntry>[
                              PopupMenuItem(
                                child: ListTile(
                                  //leading: Icon(Icons.filter_alt),
                                  title: Text(
                                    'باردة',
                                    style: TextStyle(
                                      fontSize: 11.sp,
                                      fontFamily: 'Exo',
                                      //fontWeight: FontWeight.bold,
                                      color: Colors.black,
                                    ),
                                  ),
                                  onTap: () {
                                    selectedOperationKind = 'باردة';
                                    Navigator.pop(context);
                                  },
                                ),
                              ),
                              PopupMenuItem(
                                child: ListTile(
                                  // leading: Icon(Icons.filter_alt),
                                  title: Text(
                                    'إسعافية',
                                    style: TextStyle(
                                      fontSize: 11.sp,
                                      fontFamily: 'Exo',
                                      //fontWeight: FontWeight.bold,
                                      color: Colors.black,
                                    ),
                                  ),
                                  onTap: () {
                                    selectedOperationKind = 'إسعافية';
                                    Navigator.pop(context);
                                  },
                                ),
                              ),
                            ];
                          },
                          icon: Image.asset(
                            'assets/images/filter.png',
                            fit: BoxFit.cover,
                            width: 7.w,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                Text(
                  'تاريخ العملية',
                  style: TextStyle(
                    fontSize: 11.sp,
                    fontFamily: 'Exo',
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                  ),
                ),
                CustomFields(
                  isTime:false,
                  label: '${DateFormat('yyyy/MM/dd').format(DateTime.now())}',
                  isDate: true,
                  isPhone: false,
                  icon: Padding(
                    padding: EdgeInsets.only(
                      right: 1.4.w,
                      left: 1.4.w,
                    ),
                    child: CircleAvatar(
                      backgroundColor: Colors.transparent,
                      radius: 5.sp,
                      child: Image.asset(
                        'assets/images/calendar.png',
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  controller: _dateController,
                  isNotes: false,
                  isPhoto: false,
                  validator: (String? value) {
                    if (value!.isEmpty) {
                      return 'يرجى إدخال التاريخ';
                    }
                  },
                ),
                Text(
                  'المستشفى',
                  style: TextStyle(
                    fontSize: 11.sp,
                    fontFamily: 'Exo',
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                  ),
                ),
                Row(
                  children: [
                    Expanded(
                      flex: 4,
                      child: CustomFields(
                        isTime:false,
                        label: 'مشفى المواساة',
                        isDate: false,
                        isPhone: false,
                        icon: Padding(
                          padding: EdgeInsets.only(
                            right: 1.5.w,
                            left: 1.5.w,
                          ),
                          child: CircleAvatar(
                            radius: 5.sp,
                            child: Image.asset(
                              'assets/images/hospital.png',
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        controller: _hospitalController,
                        isNotes: false,
                        isPhoto: false,
                        validator: (String? value) {
                          if (value!.isEmpty) {
                            return 'يرجى إدخال المشفى';
                          }
                        },
                      ),
                    ),
                    Expanded(
                      child: Container(
                        height: 8.8.h,
                        margin: EdgeInsets.only(right: 2.w, top: 1.h),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15),
                          color: Color(0xffEAEAEA),
                        ),
                        child: PopupMenuButton(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15.sp),
                          ),
                          itemBuilder: (BuildContext context) {
                            return <PopupMenuEntry>[
                              PopupMenuItem(
                                child: ListTile(
                                  //leading: Icon(Icons.filter_alt),
                                  title: Text(
                                    'عام',
                                    style: TextStyle(
                                      fontSize: 11.sp,
                                      fontFamily: 'Exo',
                                      //fontWeight: FontWeight.bold,
                                      color: Colors.black,
                                    ),
                                  ),
                                  onTap: () {
                                    selectedHospital = 'عام';
                                    Navigator.pop(context);
                                  },
                                ),
                              ),
                              PopupMenuItem(
                                child: ListTile(
                                  // leading: Icon(Icons.filter_alt),
                                  title: Text(
                                    'خاص',
                                    style: TextStyle(
                                      fontSize: 11.sp,
                                      fontFamily: 'Exo',
                                      //fontWeight: FontWeight.bold,
                                      color: Colors.black,
                                    ),
                                  ),
                                  onTap: () {
                                    selectedHospital = 'خاص';
                                    Navigator.pop(context);
                                  },
                                ),
                              ),
                            ];
                          },
                          icon: Image.asset(
                            'assets/images/filter.png',
                            fit: BoxFit.cover,
                            width: 7.w,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                Text(
                  'المحافظة',
                  style: TextStyle(
                    fontSize: 11.sp,
                    fontFamily: 'Exo',
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                  ),
                ),
                StatefulBuilder(
                  builder: (BuildContext context,
                      void Function(void Function()) setState) {
                    return Container(
                      height: 7.h,
                      width: 95.w,
                      margin: EdgeInsets.only(
                        top: 1.h,
                      ),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: Color(0xffEAEAEA),
                      ),
                      child: PopupMenuButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.sp),
                        ),
                        itemBuilder: (BuildContext context) {
                          return <PopupMenuEntry>[
                            PopupMenuItem(
                              child: ListTile(
                                //leading: Icon(Icons.filter_alt),
                                title: Text(
                                  'دمشق',
                                  style: TextStyle(
                                    fontSize: 11.sp,
                                    fontFamily: 'Exo',
                                    //fontWeight: FontWeight.bold,
                                    color: Colors.black,
                                  ),
                                ),
                                onTap: () {
                                  setState((){
                                    selectedCity = 'دمشق';
                                  });
                                  Navigator.pop(context);
                                },
                              ),
                            ),
                            PopupMenuItem(
                              child: ListTile(
                                //leading: Icon(Icons.filter_alt),
                                title: Text(
                                  'ريف دمشق',
                                  style: TextStyle(
                                    fontSize: 11.sp,
                                    fontFamily: 'Exo',
                                    //fontWeight: FontWeight.bold,
                                    color: Colors.black,
                                  ),
                                ),
                                onTap: () {
                                  setState((){
                                    selectedCity = 'ريف دمشق';
                                  });
                                  Navigator.pop(context);
                                },
                              ),
                            ),
                            PopupMenuItem(
                              child: ListTile(
                                //leading: Icon(Icons.filter_alt),
                                title: Text(
                                  'حلب',
                                  style: TextStyle(
                                    fontSize: 11.sp,
                                    fontFamily: 'Exo',
                                    //fontWeight: FontWeight.bold,
                                    color: Colors.black,
                                  ),
                                ),
                                onTap: () {
                                  setState((){
                                    selectedCity = 'حلب';
                                  });
                                  Navigator.pop(context);
                                },
                              ),
                            ),
                            PopupMenuItem(
                              child: ListTile(
                                //leading: Icon(Icons.filter_alt),
                                title: Text(
                                  'حماة',
                                  style: TextStyle(
                                    fontSize: 11.sp,
                                    fontFamily: 'Exo',
                                    //fontWeight: FontWeight.bold,
                                    color: Colors.black,
                                  ),
                                ),
                                onTap: () {
                                  setState((){
                                    selectedCity = 'حماة';
                                  });
                                  Navigator.pop(context);
                                },
                              ),
                            ),
                            PopupMenuItem(
                              child: ListTile(
                                // leading: Icon(Icons.filter_alt),
                                title: Text(
                                  'حمص',
                                  style: TextStyle(
                                    fontSize: 11.sp,
                                    fontFamily: 'Exo',
                                    //fontWeight: FontWeight.bold,
                                    color: Colors.black,
                                  ),
                                ),
                                onTap: () {
                                  setState((){
                                    selectedCity = 'حمص';
                                  });
                                  Navigator.pop(context);
                                },
                              ),
                            ),
                            PopupMenuItem(
                              child: ListTile(
                                // leading: Icon(Icons.filter_alt),
                                title: Text(
                                  'الحسكة',
                                  style: TextStyle(
                                    fontSize: 11.sp,
                                    fontFamily: 'Exo',
                                    //fontWeight: FontWeight.bold,
                                    color: Colors.black,
                                  ),
                                ),
                                onTap: () {
                                  setState((){
                                    selectedCity = 'الحسكة';
                                  });
                                  Navigator.pop(context);
                                },
                              ),
                            ),
                            PopupMenuItem(
                              child: ListTile(
                                // leading: Icon(Icons.filter_alt),
                                title: Text(
                                  'السويداء',
                                  style: TextStyle(
                                    fontSize: 11.sp,
                                    fontFamily: 'Exo',
                                    //fontWeight: FontWeight.bold,
                                    color: Colors.black,
                                  ),
                                ),
                                onTap: () {
                                  setState((){
                                    selectedCity = 'السويداء';
                                  });
                                  Navigator.pop(context);
                                },
                              ),
                            ),
                            PopupMenuItem(
                              child: ListTile(
                                // leading: Icon(Icons.filter_alt),
                                title: Text(
                                  'درعا',
                                  style: TextStyle(
                                    fontSize: 11.sp,
                                    fontFamily: 'Exo',
                                    //fontWeight: FontWeight.bold,
                                    color: Colors.black,
                                  ),
                                ),
                                onTap: () {
                                  setState((){
                                    selectedCity = 'درعا';
                                  });
                                  Navigator.pop(context);
                                },
                              ),
                            ),
                            PopupMenuItem(
                              child: ListTile(
                                // leading: Icon(Icons.filter_alt),
                                title: Text(
                                  'دير الزور',
                                  style: TextStyle(
                                    fontSize: 11.sp,
                                    fontFamily: 'Exo',
                                    //fontWeight: FontWeight.bold,
                                    color: Colors.black,
                                  ),
                                ),
                                onTap: () {
                                  setState((){
                                    selectedCity = 'دير الزور';
                                  });
                                  Navigator.pop(context);
                                },
                              ),
                            ),
                            PopupMenuItem(
                              child: ListTile(
                                // leading: Icon(Icons.filter_alt),
                                title: Text(
                                  'طرطوس',
                                  style: TextStyle(
                                    fontSize: 11.sp,
                                    fontFamily: 'Exo',
                                    //fontWeight: FontWeight.bold,
                                    color: Colors.black,
                                  ),
                                ),
                                onTap: () {
                                  setState((){
                                    selectedCity = 'طرطوس';
                                  });
                                  Navigator.pop(context);
                                },
                              ),
                            ),
                            PopupMenuItem(
                              child: ListTile(
                                // leading: Icon(Icons.filter_alt),
                                title: Text(
                                  'الرقة',
                                  style: TextStyle(
                                    fontSize: 11.sp,
                                    fontFamily: 'Exo',
                                    //fontWeight: FontWeight.bold,
                                    color: Colors.black,
                                  ),
                                ),
                                onTap: () {
                                  setState((){
                                    selectedCity = 'الرقة';
                                  });
                                  Navigator.pop(context);
                                },
                              ),
                            ),
                            PopupMenuItem(
                              child: ListTile(
                                // leading: Icon(Icons.filter_alt),
                                title: Text(
                                  'اللاذقية',
                                  style: TextStyle(
                                    fontSize: 11.sp,
                                    fontFamily: 'Exo',
                                    //fontWeight: FontWeight.bold,
                                    color: Colors.black,
                                  ),
                                ),
                                onTap: () {
                                  setState((){
                                    selectedCity = 'اللاذقية';
                                  });
                                  Navigator.pop(context);
                                },
                              ),
                            ),
                            PopupMenuItem(
                              child: ListTile(
                                // leading: Icon(Icons.filter_alt),
                                title: Text(
                                  'القنيطرة',
                                  style: TextStyle(
                                    fontSize: 11.sp,
                                    fontFamily: 'Exo',
                                    //fontWeight: FontWeight.bold,
                                    color: Colors.black,
                                  ),
                                ),
                                onTap: () {
                                  setState((){
                                    selectedCity = 'القنيطرة';
                                  });
                                  Navigator.pop(context);
                                },
                              ),
                            ),
                            PopupMenuItem(
                              child: ListTile(
                                // leading: Icon(Icons.filter_alt),
                                title: Text(
                                  'إدلب',
                                  style: TextStyle(
                                    fontSize: 11.sp,
                                    fontFamily: 'Exo',
                                    //fontWeight: FontWeight.bold,
                                    color: Colors.black,
                                  ),
                                ),
                                onTap: () {
                                  setState((){
                                    selectedCity = 'إدلب';
                                  });
                                  Navigator.pop(context);
                                },
                              ),
                            ),
                          ];
                        },
                        icon: Row(
                          children: [
                            CircleAvatar(
                              radius: 13.sp,
                              child: Image.asset(
                                'assets/images/google-maps.png',
                                fit: BoxFit.cover,
                              ),
                            ),
                            SizedBox(
                              width: 2.w,
                            ),
                            Text(
                              selectedCity,
                              style: TextStyle(
                                color: selectedCity == 'لم يتم الإختيار'
                                    ? Colors.black54
                                    : Colors.black,
                                fontFamily: 'Exo',
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                ),
                Text(
                  'التشريح المرضي',
                  style: TextStyle(
                    fontSize: 11.sp,
                    fontFamily: 'Exo',
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                  ),
                ),
                CustomFields(
                  isTime:false,
                  label: 'لا يوجد',
                  isDate: false,
                  isPhone: false,
                  icon: Padding(
                    padding: EdgeInsets.only(
                      right: 1.4.w,
                      left: 1.4.w,
                    ),
                    child: CircleAvatar(
                      radius: 5.sp,
                      child: Image.asset(
                        'assets/images/microscope.png',
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  controller: _histopathologyyController,
                  isNotes: false,
                  isPhoto: false,
                  validator: (String? value) {
                    if (value!.isEmpty) {
                      return 'يرجى إدخال التشريح المرضي';
                    }
                  },
                ),
                Text(
                  'هاتف المريض',
                  style: TextStyle(
                    fontSize: 11.sp,
                    fontFamily: 'Exo',
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                  ),
                ),
                CustomFields(
                  isTime:false,
                  label: '875 198 938 963+',
                  isDate: false,
                  isPhone: true,
                  icon: Padding(
                    padding: EdgeInsets.only(
                      right: 1.4.w,
                      left: 1.4.w,
                    ),
                    child: CircleAvatar(
                      radius: 5.sp,
                      backgroundColor: Colors.transparent,
                      child: Image.asset(
                        'assets/images/telephone-call.png',
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  controller: _phoneController,
                  isNotes: false,
                  isPhoto: false,
                  validator: (String? value) {
                    if (value!.isEmpty) {
                      return 'يرجى إدخال موبايل المريض';
                    }
                  },
                ),
                Text(
                  'المتابعة و الإختلاطات',
                  style: TextStyle(
                    fontSize: 11.sp,
                    fontFamily: 'Exo',
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                  ),
                ),
                CustomFields(
                  isTime:false,
                  label:
                      'نزيف , تأخر في التئام الجروح , تفاعلات سلبية مع التخدير',
                  isDate: false,
                  isPhone: false,
                  icon: Padding(
                    padding: EdgeInsets.only(
                      right: 1.4.w,
                      left: 1.4.w,
                    ),
                    child: CircleAvatar(
                      radius: 7.sp,
                      backgroundColor: Colors.transparent,
                      child: Image.asset(
                        'assets/images/to-do-list.png',
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  controller: _notesController,
                  isNotes: true,
                  isPhoto: false,
                  validator: (String? value) {
                    if (value!.isEmpty) {
                      return 'يرجى إدخال المتابعة و الاختلاطات';
                    }
                  },
                ),
                Text(
                  'صور و تحاليل',
                  style: TextStyle(
                    fontSize: 11.sp,
                    fontFamily: 'Exo',
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                  ),
                ),
                SizedBox(height: 1.h,),
                TextFormField(
                  controller: _imagesController,
                  keyboardType: TextInputType.none,
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: Color(0xffEAEAEA),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(15),
                      borderSide: BorderSide(
                        width: 0,
                        color: Color(0xffEAEAEA),
                      ),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(15),
                      borderSide: BorderSide(
                        width: 1,
                        color: Colors.blue,
                      ),
                    ),
                    labelText: 'اختر صور من الاستوديو',
                    labelStyle:  TextStyle(color: Colors.black54, fontFamily: 'Exo'),
                    prefixIcon: Padding(
                      padding:  EdgeInsets.only(left: 1.w,right: 2.w),
                      child: GestureDetector(
                        onTap: () async {
                          final List<XFile>? images =
                          await ImagePicker().pickMultiImage();
                          if (images != null) {
                            List<String> imagePaths =
                            images.map((image) => image.path).toList();
                            _imagesController.text = imagePaths.join(', ');
                            //print('beforeeeeeeeeeeee${ imagePaths.length}');
                            //Get.find<FieldsController>().addImages(imagePaths);
                            //print('afterrrrrrrrrrrrr${ imagePaths.length}');
                          }
                        },
                        child: CircleAvatar(
                          backgroundColor: Colors.transparent,
                          radius: 7.sp,
                          child: Image.asset(
                            'assets/images/camera.png',
                            fit: BoxFit.cover,
                          ),

                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 3.h,),
                MaterialButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(14.sp),
                  ),
                  minWidth: 90.w,
                  height: 7.5.h,
                  color:  Color(0xff256D85),
                  onPressed: () {
                    final name = _nameController.text;
                    final operatine = _operatineController.text;
                    final selectedOperationKindd = selectedOperationKind;
                    final date = _dateController.text;
                    final hospital = _hospitalController.text;
                    final selectedHospitall = selectedHospital;
                    final selectedCityy = selectedCity;
                    final Histopathologyy = _histopathologyyController.text;
                    final phonee = _phoneController.text;
                    final notes = _notesController.text;
                    final images = _imagesController.text;
                    final field = Field(
                      name: name,
                      operatine: operatine,
                      selectedOperationKind: selectedOperationKindd,
                      date: date,
                      hospital: hospital,
                      selectedHospital: selectedHospitall,
                      city: selectedCityy,
                      histopathology: Histopathologyy,
                      phone: phonee,
                      notes: notes,
                      images: [images],
                    );
                    if (!_formKey.currentState!.validate()) {
                    } else {

                      Get.put(FieldsController());
                      Get.find<FieldsController>().aaddField(field); // Pass the new field object to create the PDF
                      Get.to(OperatedPatients());
                    }
                  },
                  child: Text(
                    'إضافة المريض',
                    style: TextStyle(
                      fontSize: 14.sp,
                      fontFamily: 'Exo',
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
