import 'package:flutter/material.dart';
import 'package:flutter_iconly/flutter_iconly.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import '../../Controllers/FieldsController.dart';
import '../../Model/FieldData.dart';
import 'FieldDetailsPage.dart';

class FieldSearchDelegate extends SearchDelegate<String> {
  final List<Field> field;

  FieldSearchDelegate(this.field);

  String selectedFilterOption = 'الكل';

  bool fieldMatchesFilterOption(Field field, String filterOption) {
    switch (filterOption) {
      case 'الاسم':
        return field.name.toLowerCase().contains(query.toLowerCase());
      case 'العملية':
        return field.operatine.toLowerCase().contains(query.toLowerCase());
      case 'النوع':
        return field.selectedOperationKind
            .toLowerCase()
            .contains(query.toLowerCase());
      case 'التاريخ':
        return field.date.toLowerCase().contains(query.toLowerCase());
      case 'المشفى':
        return field.hospital.toLowerCase().contains(query.toLowerCase());
      case 'نوع المشفى':
        return field.selectedHospital
            .toLowerCase()
            .contains(query.toLowerCase());
      case 'المدينة':
        return field.city.toLowerCase().contains(query.toLowerCase());
      case 'الموبايل':
        return field.phone.toLowerCase().contains(query.toLowerCase());
      case 'التشريح المرضي':
        return field.histopathology.toLowerCase().contains(query.toLowerCase());
      case 'الاختلاطات':
        return field.notes.toLowerCase().contains(query.toLowerCase());
      case 'الكل':
        return field.name.toLowerCase().contains(query.toLowerCase()) ||
            field.operatine.toLowerCase().contains(query.toLowerCase()) ||
            field.selectedOperationKind
                .toLowerCase()
                .contains(query.toLowerCase()) ||
            field.date.toLowerCase().contains(query.toLowerCase()) ||
            field.hospital.toLowerCase().contains(query.toLowerCase()) ||
            field.selectedHospital
                .toLowerCase()
                .contains(query.toLowerCase()) ||
            field.city.toLowerCase().contains(query.toLowerCase()) ||
            field.phone.toLowerCase().contains(query.toLowerCase()) ||
            field.histopathology.toLowerCase().contains(query.toLowerCase()) ||
            field.notes.toLowerCase().contains(query.toLowerCase());
      default:
        return true; // No filter selected, return all fields
    }
  }

  @override
  String get searchFieldLabel => 'بحث عن مريض';

  @override
  TextStyle get searchFieldStyle => TextStyle(
        fontSize: 11.sp,
        fontFamily: 'Exo',
        fontWeight: FontWeight.bold,
        color: Colors.black54,
      );

  @override
  ThemeData appBarTheme(BuildContext context) {
    return ThemeData(
      backgroundColor: Colors.white,
      primaryColor: Colors.white,
      textTheme: TextTheme(
        headline6: TextStyle(
          fontSize: 11.sp,
          fontFamily: 'Exo',
          fontWeight: FontWeight.bold,
          color: Colors.black,
        ),
      ),
      inputDecorationTheme: InputDecorationTheme(
        fillColor: Colors.white,
        hintStyle: TextStyle(
          fontSize: 11.sp,
          fontFamily: 'Exo',
          fontWeight: FontWeight.bold,
          color: Colors.black54,
        ),
        labelStyle: TextStyle(
          fontSize: 11.sp,
          fontFamily: 'Exo',
          fontWeight: FontWeight.bold,
          color: Colors.black,
        ),
      ),
      appBarTheme: AppBarTheme(
        color: Colors.white, // specify your desired color for the app bar
        iconTheme: IconThemeData(
          color: Colors.black54,
        ), // specify icon color
      ),
    );
  }

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      SizedBox(
        width: 100,
        child: PopupMenuButton(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.sp),
          ),
          itemBuilder: (BuildContext context) {
            return <PopupMenuEntry>[
              PopupMenuItem(
                child: ListTile(
                  //leading: Icon(Icons.filter_alt),
                  title: Row(
                    children: [
                      Text(
                        'الكل ',
                        style: TextStyle(
                          fontSize: 11.sp,
                          fontFamily: 'Exo',
                          //fontWeight: FontWeight.bold,
                          color: Colors.black,
                        ),
                      ),
                    ],
                  ),
                  onTap: () {
                    selectedFilterOption = 'الكل';
                    Navigator.pop(context);
                  },
                ),
              ),
              PopupMenuItem(
                child: ListTile(
                  // leading: Icon(Icons.filter_alt),
                  title: Row(
                    children: [
                      Text(
                        'الاسم ',
                        style: TextStyle(
                          fontSize: 11.sp,
                          fontFamily: 'Exo',
                          //fontWeight: FontWeight.bold,
                          color: Colors.black,
                        ),
                      ),
                    ],
                  ),
                  onTap: () {
                    selectedFilterOption = 'الاسم';
                    Navigator.pop(context);
                  },
                ),
              ),
              PopupMenuItem(
                child: ListTile(
                  //leading: Icon(Icons.filter_alt),
                  title: Row(
                    children: [
                      Text(
                        'العملية ',
                        style: TextStyle(
                          fontSize: 11.sp,
                          fontFamily: 'Exo',
                          //fontWeight: FontWeight.bold,
                          color: Colors.black,
                        ),
                      ),
                    ],
                  ),
                  onTap: () {
                    selectedFilterOption = 'العملية';
                    Navigator.pop(context);
                  },
                ),
              ),
              PopupMenuItem(
                child: ListTile(
                  //leading: Icon(Icons.filter_alt),
                  title: Row(
                    children: [
                      Text(
                        'نوع العملية ',
                        style: TextStyle(
                          fontSize: 11.sp,
                          fontFamily: 'Exo',
                          //fontWeight: FontWeight.bold,
                          color: Colors.black,
                        ),
                      ),
                    ],
                  ),
                  onTap: () {
                    selectedFilterOption = 'النوع';
                    Navigator.pop(context);
                  },
                ),
              ),
              PopupMenuItem(
                child: ListTile(
                  //leading: Icon(Icons.filter_alt),
                  title: Text(
                    'التاريخ ',
                    style: TextStyle(
                      fontSize: 11.sp,
                      fontFamily: 'Exo',
                      //fontWeight: FontWeight.bold,
                      color: Colors.black,
                    ),
                  ),
                  onTap: () {
                    selectedFilterOption = 'التاريخ';
                    Navigator.pop(context);
                  },
                ),
              ),
              PopupMenuItem(
                child: ListTile(
                  //leading: Icon(Icons.filter_alt),
                  title: Text(
                    'المشفى ',
                    style: TextStyle(
                      fontSize: 11.sp,
                      fontFamily: 'Exo',
                      //fontWeight: FontWeight.bold,
                      color: Colors.black,
                    ),
                  ),
                  onTap: () {
                    selectedFilterOption = 'المشفى';
                    Navigator.pop(context);
                  },
                ),
              ),
              PopupMenuItem(
                child: ListTile(
                  //leading: Icon(Icons.filter_alt),
                  title: Text(
                    'نوع المشفى ',
                    style: TextStyle(
                      fontSize: 11.sp,
                      fontFamily: 'Exo',
                      //fontWeight: FontWeight.bold,
                      color: Colors.black,
                    ),
                  ),
                  onTap: () {
                    selectedFilterOption = 'نوع المشفى ';
                    Navigator.pop(context);
                  },
                ),
              ),
              PopupMenuItem(
                child: ListTile(
                  //leading: Icon(Icons.filter_alt),
                  title: Text(
                    ' المحافظة',
                    style: TextStyle(
                      fontSize: 11.sp,
                      fontFamily: 'Exo',
                      //fontWeight: FontWeight.bold,
                      color: Colors.black,
                    ),
                  ),
                  onTap: () {
                    selectedFilterOption = 'المدينة';
                    Navigator.pop(context);
                  },
                ),
              ),
              PopupMenuItem(
                child: ListTile(
                  //leading: Icon(Icons.filter_alt),
                  title: Text(
                    ' الموبايل',
                    style: TextStyle(
                      fontSize: 11.sp,
                      fontFamily: 'Exo',
                      //fontWeight: FontWeight.bold,
                      color: Colors.black,
                    ),
                  ),
                  onTap: () {
                    selectedFilterOption = 'الموبايل';
                    Navigator.pop(context);
                  },
                ),
              ),
              PopupMenuItem(
                child: ListTile(
                  //leading: Icon(Icons.filter_alt),
                  title: Text(
                    'التشريح المرضي',
                    style: TextStyle(
                      fontSize: 11.sp,
                      fontFamily: 'Exo',
                      //fontWeight: FontWeight.bold,
                      color: Colors.black,
                    ),
                  ),
                  onTap: () {
                    selectedFilterOption = 'التشريح المرضي';
                    Navigator.pop(context);
                  },
                ),
              ),
              PopupMenuItem(
                child: ListTile(
                  //leading: Icon(Icons.filter_alt),
                  title: Text(
                    'المتابعة و الاختلاطات',
                    style: TextStyle(
                      fontSize: 11.sp,
                      fontFamily: 'Exo',
                      //fontWeight: FontWeight.bold,
                      color: Colors.black,
                    ),
                  ),
                  onTap: () {
                    selectedFilterOption = 'الاختلاطات';
                    Navigator.pop(context);
                  },
                ),
              ),
            ];
          },
          icon: Transform.translate(
              offset: Offset(-3.w, 0), child: Icon(IconlyLight.filter)),
        ),
      ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: Icon(IconlyLight.arrowRight2),
      onPressed: () {
        close(context, '');
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    final filteredFields = field
        .where((field) => fieldMatchesFilterOption(field, selectedFilterOption))
        .toList();

    if (filteredFields.isEmpty) {
      return Center(
        child: Text(
          "لا يوجد مريض بهذه المعلومات",
          style: TextStyle(fontSize: 11.sp, fontFamily: 'Exo'),
        ),
      );
    }

    return ListView.builder(
      itemCount: filteredFields.length,
      itemBuilder: (context, index) {
        final field = filteredFields[index];

        return GestureDetector(
          onTap: () {
            Get.to(FieldDetailsPage(field: field));
          },
          child: Container(
            margin: EdgeInsets.only(
              left: 3.w,
              right: 3.w,
              bottom: 2.h,
              top: 2.h,
            ),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10.sp),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.7),
                  blurRadius: 6.0,
                  offset: Offset(0, 2),
                ),
              ],
            ),
            child: ListTile(
              title: Stack(
                children: [
                  Text(
                    field.name,
                    style: TextStyle(
                      fontSize: 11.sp,
                      fontFamily: 'Exo',
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 1.h,
                  ),
                  Row(
                    children: [
                      CircleAvatar(
                        radius: 11.sp,
                        child: Image.asset(
                          'assets/images/surgery-tools.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                      SizedBox(
                        width: 2.w,
                      ),
                      Text(
                        field.operatine,
                        style: TextStyle(
                          fontSize: 11.sp,
                          fontFamily: 'Exo',
                          color: Colors.grey[600],
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 1.h,
                  ),
                  Row(
                    children: [
                      CircleAvatar(
                        radius: 11.sp,
                        child: Image.asset(
                          'assets/images/google-maps.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                      SizedBox(
                        width: 2.w,
                      ),
                      Text(
                        field.hospital,
                        style: TextStyle(
                          fontSize: 11.sp,
                          fontFamily: 'Exo',
                          color: Colors.grey[600],
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 1.h,
                  ),
                  Row(
                    children: [
                      CircleAvatar(
                        radius: 11.sp,
                        child: Image.asset(
                          'assets/images/calendar.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                      SizedBox(
                        width: 2.w,
                      ),
                      Text(
                        field.date,
                        style: TextStyle(
                          fontSize: 11.sp,
                          fontFamily: 'Exo',
                          color: Colors.grey[600],
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 1.h,
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    final filteredFields = field
        .where((field) => fieldMatchesFilterOption(field, selectedFilterOption))
        .toList();

    if (filteredFields.isEmpty) {
      return Center(
        child: Text(
          'لا يوجد مريض بهذه المعلومات',
          style: TextStyle(
              fontSize: 11.sp,
              fontFamily: 'Exo',
              fontWeight: FontWeight.bold,
              color: Colors.black54,
              letterSpacing: 1),
        ),
      );
    }

    return ListView.builder(
      itemCount: filteredFields.length,
      itemBuilder: (context, index) {
        final field = filteredFields[index];

        return GestureDetector(
          onTap: () {
            Get.to(FieldDetailsPage(field: field));
          },
          child: Container(
            margin: EdgeInsets.only(left: 3.w, right: 3.w, bottom: 3.h),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10.sp),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.7),
                  blurRadius: 6.0,
                  offset: Offset(0, 2),
                ),
              ],
            ),
            child: ListTile(
              title: Stack(
                children: [
                  Text(
                    field.name,
                    style: TextStyle(
                      fontSize: 11.sp,
                      fontFamily: 'Exo',
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 1.h,
                  ),
                  Row(
                    children: [
                      CircleAvatar(
                        radius: 11.sp,
                        child: Image.asset(
                          'assets/images/surgery-tools.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                      SizedBox(
                        width: 2.w,
                      ),
                      Text(
                        field.operatine,
                        style: TextStyle(
                          fontSize: 11.sp,
                          fontFamily: 'Exo',
                          color: Colors.grey[600],
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 1.h,
                  ),
                  Row(
                    children: [
                      CircleAvatar(
                        radius: 11.sp,
                        child: Image.asset(
                          'assets/images/google-maps.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                      SizedBox(
                        width: 2.w,
                      ),
                      Text(
                        field.hospital,
                        style: TextStyle(
                          fontSize: 11.sp,
                          fontFamily: 'Exo',
                          color: Colors.grey[600],
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 1.h,
                  ),
                  Row(
                    children: [
                      CircleAvatar(
                        radius: 11.sp,
                        child: Image.asset(
                          'assets/images/calendar.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                      SizedBox(
                        width: 2.w,
                      ),
                      Text(
                        field.date,
                        style: TextStyle(
                          fontSize: 11.sp,
                          fontFamily: 'Exo',
                          color: Colors.grey[600],
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 1.h,
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
