import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_iconly/flutter_iconly.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:photo_view/photo_view_gallery.dart';
import 'package:sizer/sizer.dart';
import '../../Controllers/FieldsController.dart';
import '../../main.dart';
import 'dart:io';
import 'package:photo_view/photo_view.dart';

import 'AllImages.dart';
import 'Home.dart';

class FieldDetailsPage extends StatelessWidget {
  final Field field;

  const FieldDetailsPage({Key? key, required this.field}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String? path = sharedPref?.getString("path");
    List<String> paths = field.images[0].split(', ');
    return Scaffold(
      appBar: AppBar(
        //toolbarHeight: 10.h,
        backgroundColor: Color(0xff256D85),
        title: Text(
          field.name,
          style: TextStyle(
            fontSize: 13.sp,
            fontFamily: 'Exo',
            fontWeight: FontWeight.bold,
            letterSpacing: 0.1.w,
            color: Colors.white,
          ),
        ),
        leading: IconButton(
          onPressed: () {
            Get.back();
          },
          icon: Icon(IconlyLight.arrowRight2),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              width: 100.w,
              height: field.images[0].split(', ').length <=2?27.h:53.h,
              padding: EdgeInsets.symmetric(
                horizontal: 1.w,
                vertical: 1.h,
              ),
              child: GridView.builder(
                itemCount: field.images[0].split(', ').length == 1
                    ? 1
                    : field.images[0].split(', ').length == 2
                        ? 2
                        : field.images[0].split(', ').length == 3
                            ? 3
                            : 4,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2, // Number of columns
                  mainAxisSpacing: 10, // Spacing between rows
                  crossAxisSpacing: 10, // Spacing between columns
                ),
                itemBuilder: (c, i) {
                  return GestureDetector(
                      onTap: () {
                        i == 3
                            ? Get.to(AllImages(field: field))
                            :showDialog(
                          context: context,
                          builder: (c) {
                            return GestureDetector(
                              onTap: () => Navigator.of(context).pop(),
                              child: Stack(
                                children: [
                                  Container(
                                    height: double.infinity,
                                    width: double.infinity,
                                    color: Colors.black,
                                  ),
                                  Container(
                                    alignment: Alignment.topCenter,
                                    height: 0.5.h,
                                    color: Colors.black,
                                    width: double.infinity,
                                    child: BackdropFilter(
                                      filter: ImageFilter.blur(
                                          sigmaX: 5, sigmaY: 5),
                                      child: Container(
                                          color: Colors.transparent),
                                    ),
                                  ),
                                  // Add icon button here
                                  Positioned(
                                    top: 0,
                                    right: 0,
                                    child: Material(
                                      child: IconButton(
                                        icon: Icon(Icons.close),
                                        onPressed: () => Navigator.of(context).pop(),
                                      ),
                                    ),
                                  ),
                                  Container(
                                    alignment: Alignment.bottomCenter,
                                    height: 0.5.h,
                                    width: double.infinity,
                                    color: Colors.transparent,
                                    child: BackdropFilter(
                                      filter: ImageFilter.blur(
                                          sigmaX: 5, sigmaY: 5),
                                      child: Container(
                                        color: Colors.transparent,
                                      ),
                                    ),
                                  ),
                                  Center(
                                    child: Container(
                                      height: double.infinity,
                                      width: double.infinity,
                                      color: Colors.transparent,
                                      child: PhotoView(
                                        imageProvider: FileImage(
                                          File(paths[i]),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            );
                          },
                        );
                      },
                      child: Stack(
                        alignment: Alignment.center,
                        children: [
                          Container(
                            margin: EdgeInsets.all(0.1),
                            color: Colors.transparent,
                            child: Image.file(
                              File(paths[i]),
                              width: double.infinity,
                              height: 80.h,
                              fit: BoxFit.cover,
                            ),
                          ),
                          i == 3
                              ? GestureDetector(
                                  onTap: () {
                                    Get.to(AllImages(
                                      field: field,
                                    ));
                                  },
                                  child: Text(
                                    '+${field.images[0].split(', ').length - 4}',
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 35.sp,
                                    ),
                                  ),
                                )
                              : Text(''),
                        ],
                      ));
                },
              ),

              // ListView.builder(
              //       scrollDirection: Axis.horizontal,
              //         itemCount: field.images[0].split(', ').length,
              //         itemBuilder: (c, i) {
              //           return GestureDetector(
              //             onTap: () {
              //               showDialog(
              //                 context: context,
              //                 builder: (c) {
              //                   return GestureDetector(
              //                     onTap: () => Navigator.of(context).pop(),
              //                     child: Stack(
              //                       children: [
              //                         Container(
              //                           height: double.infinity,
              //                           width: double.infinity,
              //                           color: Colors.transparent,
              //                         ),
              //                         Container(
              //                           alignment: Alignment.topCenter,
              //                           height: 0.5.h,
              //                           width: double.infinity,
              //                           child: BackdropFilter(
              //                             filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
              //                             child: Container(color: Colors.black.withOpacity(0.5)),
              //                           ),
              //                         ),
              //                         Container(
              //                           alignment: Alignment.bottomCenter,
              //                           height: 0.5.h,
              //                           width: double.infinity,
              //                           child: BackdropFilter(
              //                             filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
              //                             child: Container(color: Colors.black.withOpacity(0.5)),
              //                           ),
              //                         ),
              //                         Center(
              //                           child: Container(
              //                             height: 25.h,
              //                             width: 100.w,
              //                             child: PhotoView(
              //                               imageProvider: FileImage(
              //                                 File(paths[i]),
              //                               ),
              //                             ),
              //                           ),
              //                         ),
              //                       ],
              //                     ),
              //                   );
              //                 },
              //               );
              //             },
              //             child: Container(
              //               margin: EdgeInsets.all(8.0),
              //               child: Image.file(
              //                 File(paths[i]),
              //                 width: 200.0,
              //                 height: 200.0,
              //                 fit: BoxFit.cover,
              //               ),
              //             ),
              //           );
              //         }),
            ),
            SizedBox(
              height: 0.h,
            ),
            Container(
              margin: EdgeInsets.only(
                right: 4.w,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 2.h),
                  Row(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(
                          right: 1.5.w,
                          left: 1.5.w,
                        ),
                        child: CircleAvatar(
                          radius: 11.sp,
                          child: Image.asset(
                            'assets/images/surgery-tools.png',
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      SizedBox(width: 2.w),
                      Row(
                        children: [
                          Text(
                            'العملية : ${field.operatine} ',
                            style: TextStyle(
                              fontSize: 11.sp,
                              fontFamily: 'Exo',
                              fontWeight: FontWeight.bold,
                              letterSpacing: 0.1.w,
                              color: Colors.black,
                            ),
                          ),
                          SizedBox(
                            width: 1.w,
                          ),
                          Text(
                            '( ${field.selectedOperationKind} )',
                            style: TextStyle(
                              fontSize: 11.sp,
                              fontFamily: 'Exo',
                              fontWeight: FontWeight.bold,
                              letterSpacing: 0.1.w,
                              color: field.selectedOperationKind == 'باردة'
                                  ? Colors.green
                                  : field.selectedOperationKind ==
                                          'لم يتم الإختيار'
                                      ? Colors.black54
                                      : Colors.red,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(height: 2.h),
                  Row(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(
                          right: 1.5.w,
                          left: 1.5.w,
                        ),
                        child: CircleAvatar(
                          radius: 11.sp,
                          child: Image.asset(
                            'assets/images/calendar.png',
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      SizedBox(width: 2.w),
                      Text(
                        'التاريخ : ${DateFormat('yyyy/MM/dd').format(DateTime.parse(field.date))}',
                        style: TextStyle(
                          fontSize: 11.sp,
                          fontFamily: 'Exo',
                          fontWeight: FontWeight.bold,
                          letterSpacing: 0.1.w,
                          color: Colors.black,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 2.h),
                  Row(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(
                          right: 1.5.w,
                          left: 1.5.w,
                        ),
                        child: CircleAvatar(
                          radius: 11.sp,
                          child: Image.asset(
                            'assets/images/hospital.png',
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      SizedBox(width: 2.w),
                      Flexible(
                        child: Row(
                          children: [
                            Text(
                              'المشفى : ${field.hospital}',
                              style: TextStyle(
                                fontSize: 11.sp,
                                fontFamily: 'Exo',
                                fontWeight: FontWeight.bold,
                                letterSpacing: 0.1.w,
                                color: Colors.black,
                              ),
                            ),
                            SizedBox(
                              width: 2.w,
                            ),
                            Flexible(
                              child: Text(
                                '( ${field.selectedHospital} )',
                                style: TextStyle(
                                  fontSize: 11.sp,
                                  fontFamily: 'Exo',
                                  fontWeight: FontWeight.bold,
                                  letterSpacing: 0.1.w,
                                  color: field.selectedHospital == 'عام'
                                      ? Colors.green
                                      : field.selectedHospital ==
                                              'لم يتم الإختيار'
                                          ? Colors.black54
                                          : Colors.red,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 2.h),
                  Row(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(
                          right: 1.5.w,
                          left: 1.5.w,
                        ),
                        child: CircleAvatar(
                          radius: 11.sp,
                          child: Image.asset(
                            'assets/images/google-maps.png',
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      SizedBox(width: 2.w),
                      Text(
                        'المحافظة : ${field.city}',
                        style: TextStyle(
                          fontSize: 11.sp,
                          fontFamily: 'Exo',
                          fontWeight: FontWeight.bold,
                          letterSpacing: 0.1.w,
                          color: Colors.black,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 2.h),
                  Container(
                    margin: EdgeInsets.only(
                      left: 5.w,
                    ),
                    child: Row(
                      children: [
                        Padding(
                          padding: EdgeInsets.only(
                            right: 1.5.w,
                            left: 1.5.w,
                          ),
                          child: CircleAvatar(
                            radius: 11.sp,
                            child: Image.asset(
                              'assets/images/microscope.png',
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        SizedBox(width: 2.w),
                        Flexible(
                          child: Text(
                            ' التشريح المرضي : ${field.histopathology}',
                            style: TextStyle(
                              fontSize: 11.sp,
                              fontFamily: 'Exo',
                              fontWeight: FontWeight.bold,
                              letterSpacing: 0.1.w,
                              color: Colors.black,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 2.h),
                  Row(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(
                          right: 1.5.w,
                          left: 1.5.w,
                        ),
                        child: CircleAvatar(
                          radius: 11.sp,
                          child: Image.asset(
                            'assets/images/to-do-list.png',
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      SizedBox(width: 2.w),
                      Flexible(
                        child: Text(
                          ' المتابعة و الإختلاطات : ${field.notes}',
                          style: TextStyle(
                            fontSize: 11.sp,
                            fontFamily: 'Exo',
                            fontWeight: FontWeight.bold,
                            letterSpacing: 0.1.w,
                            color: Colors.black,
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 4.h),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
