import 'package:flutter/material.dart';
import 'package:flutter_iconly/flutter_iconly.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:url_launcher/url_launcher.dart';
import 'OperatedPatients.dart';
import 'new patients.dart';

class About extends StatelessWidget {

  _launchEmail() async {
    final Uri params = Uri(
      scheme: 'mailto',
      path: 'almoaead77@gmail.com',
    );
    String url = params.toString();
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
  void _launchFacebook() async{
    var url = 'fb://facewebmodal/f?href=https://www.facebook.com/profile.php?id=100015038264200';
    if (await canLaunch(url)) {
      await launch( url, universalLinksOnly: true, );
    } else { throw 'There was a problem to open the url: $url'; }

  }

  _launchWhatsApp() async {
    final String phone = '+963968557674';
    final String message = 'hi';
    final String url = 'whatsapp://send?phone=$phone&text=$message';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }


  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          width: double.infinity,
          height: double.infinity,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/images/wall.jpg'),
              fit: BoxFit.cover,
              colorFilter: ColorFilter.mode(
                Colors.white.withOpacity(0.5), // set the opacity level
                BlendMode.dstIn, // set the blend mode
              ),
            ),
          ),
          child: Stack(
            alignment: Alignment.center,
            children: [
              Positioned(
                top: 10.h,
                left: Get.width / 3,
                child: CircleAvatar(
                  foregroundImage: AssetImage('assets/images/surgery.jpg'),
                  radius: 50.sp,
                ),
              ),
              Transform.translate(
                offset: Offset(-22.w, -16.h),
                child: Container(
                  width: 90.w,
                  child: Text(
                    'MY PATIENTS',
                    style: TextStyle(
                      fontSize: 20.sp,
                      fontFamily: 'AdventPro',
                      fontWeight: FontWeight.bold,
                      letterSpacing: 0.5.w,
                    ),
                  ),
                ),
              ),
              Transform.translate(
                offset: Offset(20.w, -44.h),
                child: GestureDetector(
                  onTap: () {
                    Get.back();
                  },
                  child: Container(
                    width: 50.w,
                    child: Row(
                      children: [
                        Icon(
                          IconlyLight.arrowRight2,
                          size: 20.sp,
                        ),
                        Text(
                          'رجوع ',
                          style: TextStyle(
                            fontSize: 12.sp,
                            fontFamily: 'Exo',
                            fontWeight: FontWeight.bold,
                            letterSpacing: 0.5.w,
                            color: Colors.black,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Positioned(
                top: 38.h,
                child: Container(
                  alignment: Alignment.center,
                  width: 90.w,
                  child: Text(
                    'يسعدنا أن نقدم لك تطبيقنا الجديد الذي يسهل عليك متابعة مواعيد مرضاك بكل سهولة ويسر. يقوم التطبيق بتوفير خدمة الأرشفة الإلكترونية للمرضى، وتلقي إشعارات فورية عندما تحجز موعدًا لمريض جديد .',
                    style: TextStyle(
                      color: Colors.black54,
                      fontSize: 13.sp,
                      fontWeight: FontWeight.w400,
                      fontFamily: "Exo",
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              Positioned(
                top: 64.h,
                child: Container(
                  alignment: Alignment.center,
                  width: 90.w,
                  child: Text(
                    'اذا كان لديكم أي أسئلة أو استفسارات، فلا تترددوا في التواصل معي عبر ',
                    style: TextStyle(
                      color: Colors.black87,
                      fontSize: 13.sp,
                      fontWeight: FontWeight.w600,
                      fontFamily: "Exo",
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              Positioned(
                top: 76.h,
                left: -7.w,
                child: GestureDetector(
                  onTap: (){
                    _launchEmail();
                  },
                  child: Container(
                    alignment: Alignment.center,
                    width: 90.w,
                    child: Row(
                      children: [
                        Text(
                          'almoaead77@gmail.com',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 13.sp,
                            fontWeight: FontWeight.w400,
                            fontFamily: "Exo",
                          ),
                          textAlign: TextAlign.center,
                        ),
                        SizedBox(
                          width: 2.w,
                        ),
                        Image.asset(
                          'assets/images/gmail.png',
                          height: 4.h,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Positioned(
                top: 82.h,
                left: -21.w,
                child: GestureDetector(
                  onTap: (){
                    _launchFacebook();
                  },
                  child: Container(
                    alignment: Alignment.center,
                    width: 90.w,
                    child: Row(
                      children: [
                        Text(
                          'Al Mouayad Shwin',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 13.sp,
                            fontWeight: FontWeight.w400,
                            fontFamily: "Exo",
                          ),
                          textAlign: TextAlign.center,
                        ),
                        SizedBox(
                          width: 2.w,
                        ),
                        Image.asset(
                          'assets/images/facebook.png',
                          height: 4.h,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Positioned(
                top: 88.h,
                left: -20.w,
                child: GestureDetector(
                  onTap: (){
                    _launchWhatsApp();
                  },
                  child: Container(
                    alignment: Alignment.center,
                    width: 90.w,
                    child: Row(
                      children: [
                        Text(
                          '674 557 968 963+',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 13.sp,
                            fontWeight: FontWeight.w400,
                            fontFamily: "Exo",
                          ),
                          textAlign: TextAlign.center,
                        ),
                        SizedBox(
                          width: 2.w,
                        ),
                        Image.asset(
                          'assets/images/whatsapp.png',
                          height: 4.h,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Positioned(
                top: 94.h,
                left: 41.w,
                child:  Text(
                  'version 1.0.0',
                  style: TextStyle(
                    color: Colors.black54,
                    fontSize: 10.sp,
                    fontWeight: FontWeight.w400,
                    //fontFamily: "Exo",
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
