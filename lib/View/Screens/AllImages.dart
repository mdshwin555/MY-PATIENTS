import 'dart:io';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter_iconly/flutter_iconly.dart';
import 'package:get/get.dart';
import 'package:photo_view/photo_view.dart';
import 'package:sizer/sizer.dart';
import '../../Controllers/FieldsController.dart';

class AllImages extends StatelessWidget {
  final Field field;

  const AllImages({Key? key, required this.field}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<String> paths = field.images[0].split(', ');
    return Scaffold(
      appBar: AppBar(
        //toolbarHeight: 10.h,
        backgroundColor: Color(0xff256D85),
        title: Text(
          'جميع الصور ',
          style: TextStyle(
            fontSize: 14.sp,
            fontFamily: 'Exo',
            fontWeight: FontWeight.bold,
            letterSpacing: 0.1.w,
            color: Colors.white,
          ),
        ),
        leading: IconButton(
          onPressed: () {
            Get.back();
          },
          icon: Icon(IconlyLight.arrowRight2),
        ),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(
          horizontal: 1.w,
          vertical: 1.h,
        ),
        height: 100.h,
        width: 100.w,
        child: GridView.builder(
          itemCount: field.images[0].split(', ').length,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2, // Number of columns
            mainAxisSpacing: 10, // Spacing between rows
            crossAxisSpacing: 10, // Spacing between columns
          ),
          itemBuilder: (c, i) {
            return GestureDetector(
              onTap: () {
                showDialog(
                  context: context,
                  builder: (c) {
                    return GestureDetector(
                      onTap: () => Navigator.of(context).pop(),
                      child: Stack(
                        children: [
                          Container(
                            height: double.infinity,
                            width: double.infinity,
                            color: Colors.transparent,
                          ),
                          Container(
                            alignment: Alignment.topCenter,
                            height: 0.5.h,
                            width: double.infinity,
                            child: BackdropFilter(
                              filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
                              child: Container(
                                  color: Colors.black.withOpacity(0.5)),
                            ),
                          ),
                          Container(
                            alignment: Alignment.bottomCenter,
                            height: 0.5.h,
                            width: double.infinity,
                            child: BackdropFilter(
                              filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
                              child: Container(
                                  color: Colors.black.withOpacity(0.5)),
                            ),
                          ),
                          Center(
                              child: Container(
                            height: 25.h,
                            width: 100.w,
                            child: PhotoView(
                              imageProvider: FileImage(
                                File(paths[i]),
                              ),
                            ),
                          )),
                        ],
                      ),
                    );
                  },
                );
              },
              child: Container(
                margin: EdgeInsets.all(0.01.w),
                child: Image.file(
                  File(paths[i]),
                  width: 200.0,
                  height: 200.0,
                  fit: BoxFit.cover,
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
