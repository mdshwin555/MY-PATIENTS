import 'dart:typed_data';
import 'package:flutter/services.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_iconly/flutter_iconly.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:serg_log2/View/Screens/searchPage.dart';
import 'package:sizer/sizer.dart';
import '../../Controllers/FieldsController.dart';
import 'FieldDetailsPage.dart';
import 'FieldEditorScreen.dart';
import 'Home.dart';
import 'package:syncfusion_flutter_pdf/pdf.dart';

import 'mobile.dart';

class OperatedPatients extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        backgroundColor: Color(0xff256D85),
        title: Text(
          'المرضى المشغولين',
          style: TextStyle(
            fontSize: 12.sp,
            fontFamily: 'Exo',
            fontWeight: FontWeight.bold,
            letterSpacing: 0.1.w,
            color: Colors.white,
          ),
        ),
        leading: IconButton(
          onPressed: () {
            Get.to(Home());
          },
          icon: Icon(IconlyLight.arrowRight2),
        ),
        actions: [
          IconButton(
            onPressed: () {
              _createPDF();
            },
            icon: Icon(IconlyLight.paperDownload),
          ),
          IconButton(
            onPressed: () {
              showSearch(
                context: context,
                delegate:
                    FieldSearchDelegate(Get.find<FieldsController>().fields),
              );
            },
            icon: Icon(IconlyLight.search),
          ),
        ],
        //centerTitle: true,
      ),
      body: GetX<FieldsController>(
        init: FieldsController(),
        builder: (controller) {
          final fields = controller.fields;
          return controller.fields.length == 0
              ? Center(
                  child: Text(
                    'لا يوجد مرضى بعد',
                    style: TextStyle(
                      fontSize: 17.sp,
                      fontFamily: 'Exo',
                      fontWeight: FontWeight.bold,
                      letterSpacing: 0.1.w,
                      color: Colors.black54,
                    ),
                  ),
                )
              : Column(
                  children: [
                    SizedBox(
                      height: 1.h,
                    ),
                    Text(
                      fields.length == 1
                          ? 'لديك مريض واحد'
                          : fields.length == 2
                              ? 'لديك مريضين '
                              : fields.length >= 3 && fields.length <= 10
                                  ? 'لديك ${fields.length} مرضى'
                                  : 'لديك ${fields.length} مريض',
                      style: TextStyle(
                        fontSize: 12.sp,
                        fontFamily: 'Exo',
                        //fontWeight: FontWeight.bold,
                      ),
                    ),
                    Flexible(
                      child: Container(
                        height: 100.h,
                        width: 100.w,
                        margin: EdgeInsets.only(
                          top: 2.h,
                        ),
                        child: ListView.separated(
                          itemCount: fields.length,
                          itemBuilder: (context, index) {
                            final field = fields[index];

                            return GestureDetector(
                              onTap: () {
                                Get.to(FieldDetailsPage(field: field));
                              },
                              child: Container(
                                margin: EdgeInsets.only(
                                  left: 3.w,
                                  right: 3.w,
                                  bottom: 3.h,
                                ),
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10.sp),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.grey.withOpacity(0.7),
                                      blurRadius: 6.0,
                                      offset: Offset(0, 2),
                                    ),
                                  ],
                                ),
                                child: Container(
                                  child: ListTile(
                                    title: Stack(
                                      children: [
                                        Container(
                                          height:5.h,
                                          child: Text(
                                            field.name,
                                            style: TextStyle(
                                              fontSize: 11.sp,
                                              fontFamily: 'Exo',
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ),
                                        Positioned(
                                          left: 10.5.w,
                                          top: 0.h,
                                          child: IconButton(
                                            onPressed: () async {
                                              final result = await Get.to(
                                                  FieldEditorScreen(
                                                      field: fields[index]));
                                              if (result != null) {
                                                controller.editField(
                                                    result, fields[index]);
                                              }
                                            },
                                            icon: Icon(
                                              IconlyBroken.edit,
                                              color: Colors.green[500],
                                              size: 23.sp,
                                            ),
                                          ),
                                        ),
                                        Positioned(
                                          left: -2.5.w,
                                          top: 0.h,
                                          child: IconButton(
                                            onPressed: () {
                                              showDialog(
                                                context: context,
                                                builder: (BuildContext context) {
                                                  return Dialog(
                                                    shape: RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              16),
                                                    ),
                                                    child: Padding(
                                                      padding:
                                                          const EdgeInsets.all(
                                                              24),
                                                      child: Column(
                                                        mainAxisSize:
                                                            MainAxisSize.min,
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: [
                                                          Row(
                                                            children: [
                                                              CircleAvatar(
                                                                radius: 15.sp,
                                                                backgroundColor:
                                                                    Colors
                                                                        .red[400],
                                                                child: Icon(
                                                                  IconlyLight
                                                                      .delete,
                                                                  color: Colors
                                                                      .white,
                                                                ),
                                                              ),
                                                              SizedBox(
                                                                width: 2.w,
                                                              ),
                                                              Text(
                                                                'حذف المريض',
                                                                style: TextStyle(
                                                                  fontSize: 13.sp,
                                                                  fontFamily:
                                                                      'Exo',
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                  letterSpacing:
                                                                      0.1.w,
                                                                  color: Colors
                                                                      .black,
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                          SizedBox(height: 16),
                                                          Text(
                                                            'هل أنت متأكد من رغبتك في حذف هذا المريض ؟',
                                                            style: TextStyle(
                                                              fontSize: 10.sp,
                                                              fontFamily: 'Exo',
                                                              color:
                                                                  Colors.black54,
                                                            ),
                                                          ),
                                                          SizedBox(height: 24),
                                                          Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .end,
                                                            children: [
                                                              Expanded(
                                                                child:
                                                                    MaterialButton(
                                                                  child: Text(
                                                                    'لا',
                                                                    style:
                                                                        TextStyle(
                                                                      fontSize:
                                                                          10.sp,
                                                                      fontFamily:
                                                                          'Exo',
                                                                      //fontWeight: FontWeight.bold,
                                                                      //letterSpacing: 0.1.w,
                                                                      color: Colors
                                                                          .black,
                                                                    ),
                                                                  ),
                                                                  color: Colors
                                                                      .white,
                                                                  shape:
                                                                      RoundedRectangleBorder(
                                                                    borderRadius:
                                                                        BorderRadius
                                                                            .circular(
                                                                                8.0),
                                                                    side:
                                                                        BorderSide(
                                                                      color: Colors
                                                                          .black,
                                                                      width:
                                                                          0.2.w,
                                                                    ),
                                                                  ),
                                                                  onPressed: () {
                                                                    Navigator.of(
                                                                            context)
                                                                        .pop();
                                                                  },
                                                                ),
                                                              ),
                                                              SizedBox(width: 16),
                                                              Expanded(
                                                                child:
                                                                    MaterialButton(
                                                                  child: Text(
                                                                    'نعم بالتأكيد',
                                                                    style:
                                                                        TextStyle(
                                                                      fontSize:
                                                                          10.sp,
                                                                      fontFamily:
                                                                          'Exo',
                                                                      //fontWeight: FontWeight.bold,
                                                                      //letterSpacing: 0.1.w,
                                                                      color: Colors
                                                                          .white,
                                                                    ),
                                                                  ),
                                                                  color: Colors
                                                                      .red[400],
                                                                  shape:
                                                                      RoundedRectangleBorder(
                                                                    borderRadius:
                                                                        BorderRadius
                                                                            .circular(
                                                                                8.0),
                                                                  ),
                                                                  onPressed: () {
                                                                    Navigator.of(
                                                                            context)
                                                                        .pop();
                                                                    Get.find<
                                                                            FieldsController>()
                                                                        .deleteField(
                                                                            index);
                                                                  },
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  );
                                                },
                                              );
                                            },
                                            icon: Icon(
                                              CupertinoIcons.delete,
                                              color: Colors.red[400],
                                              size: 22.sp,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    subtitle: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        SizedBox(
                                          height: 1.h,
                                        ),
                                        Row(
                                          children: [
                                            CircleAvatar(
                                              radius: 11.sp,
                                              child: Image.asset(
                                                'assets/images/surgery-tools.png',
                                                fit: BoxFit.cover,
                                              ),
                                            ),
                                            SizedBox(
                                              width: 2.w,
                                            ),
                                            Text(
                                              field.operatine,
                                              style: TextStyle(
                                                fontSize: 12.sp,
                                                fontFamily: 'Exo',
                                                color: Colors.grey[600],
                                              ),
                                            ),
                                          ],
                                        ),
                                        SizedBox(
                                          height: 1.h,
                                        ),
                                        Row(
                                          children: [
                                            CircleAvatar(
                                              radius: 11.sp,
                                              child: Image.asset(
                                                'assets/images/hospital.png',
                                                fit: BoxFit.cover,
                                              ),
                                            ),
                                            SizedBox(
                                              width: 2.w,
                                            ),
                                            Text(
                                              field.hospital,
                                              style: TextStyle(
                                                fontSize: 12.sp,
                                                fontFamily: 'Exo',
                                                color: Colors.grey[600],
                                              ),
                                            ),
                                          ],
                                        ),
                                        SizedBox(
                                          height: 1.h,
                                        ),
                                        Row(
                                          children: [
                                            CircleAvatar(
                                              radius: 11.sp,
                                              child: Image.asset(
                                                'assets/images/calendar.png',
                                                fit: BoxFit.cover,
                                              ),
                                            ),
                                            SizedBox(
                                              width: 2.w,
                                            ),
                                            Text(
                                              DateFormat('yyyy/MM/dd').format(
                                                  DateTime.parse(field.date)),
                                              style: TextStyle(
                                                fontSize: 12.sp,
                                                fontFamily: 'Exo',
                                                color: Colors.grey[600],
                                              ),
                                            ),
                                          ],
                                        ),
                                        SizedBox(
                                          height: 1.h,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            );
                          },
                          separatorBuilder: (BuildContext context, int index) {
                            return SizedBox(
                              height: 0.h,
                            );
                          },
                        ),
                      ),
                    ),
                  ],
                );
        },
      ),
    );
  }

  Future<void> _createPDF() async {
    // Load the Arabic font file
    final ByteData fontData =
        await rootBundle.load('assets/fonts/Amiri-Regular.ttf');
    final arabicFont = PdfTrueTypeFont(
        fontData.buffer
            .asUint8List(fontData.offsetInBytes, fontData.lengthInBytes),
        12);
    final fieldsController = Get.put(FieldsController());
    final fieldsControllerr = Get.find<FieldsController>();

    // Create the PDF grid and set the font style
    PdfGrid grid = PdfGrid();
    grid.style = PdfGridStyle(
      font: arabicFont, // Use the Arabic font
      cellPadding: PdfPaddings(left: 5, right: 5, top: 5, bottom: 5),
      backgroundBrush: PdfSolidBrush(PdfColor(240, 240, 240)),
    );

    // Add grid columns and headers
    grid.columns.add(count: 5);
    grid.headers.add(1);

    // Set the header text and format
    PdfGridRow header = grid.headers[0];
    header.cells[0].value = 'المدينة';
    header.cells[0].stringFormat.alignment = PdfTextAlignment.center;
    header.cells[0].stringFormat.textDirection = PdfTextDirection.rightToLeft;
    header.cells[1].value = 'المشفى';
    header.cells[1].stringFormat.alignment = PdfTextAlignment.center;
    header.cells[1].stringFormat.textDirection = PdfTextDirection.rightToLeft;
    header.cells[2].value = 'التاريخ';
    header.cells[2].stringFormat.alignment = PdfTextAlignment.center;
    header.cells[2].stringFormat.textDirection = PdfTextDirection.rightToLeft;
    header.cells[3].value = 'العملية';
    header.cells[3].stringFormat.alignment = PdfTextAlignment.center;
    header.cells[3].stringFormat.textDirection = PdfTextDirection.rightToLeft;
    header.cells[4].value = 'الاسم';
    header.cells[4].stringFormat.alignment = PdfTextAlignment.center;
    header.cells[4].stringFormat.textDirection = PdfTextDirection.rightToLeft;
    for (int j = 1; j <= fieldsController.fields.length; j++) {
      for (int i = j - 1; i <= j - 1; i++) {
        PdfGridRow row = grid.rows.add();
        row.cells[4].value = '${fieldsController.fields[i].name}';
        row.cells[4].stringFormat.alignment = PdfTextAlignment.center;
        row.cells[4].stringFormat.textDirection = PdfTextDirection.rightToLeft;
        row.cells[3].value =
            '${fieldsController.fields[i].operatine}  ( ${fieldsController.fields[i].selectedOperationKind} )';
        row.cells[3].stringFormat.alignment = PdfTextAlignment.center;
        row.cells[3].stringFormat.textDirection = PdfTextDirection.rightToLeft;
        row.cells[2].value = '${fieldsController.fields[i].date}';
        row.cells[2].stringFormat.alignment = PdfTextAlignment.center;
        row.cells[2].stringFormat.textDirection = PdfTextDirection.rightToLeft;
        row.cells[1].value =
            '${fieldsController.fields[i].hospital} ( ${fieldsController.fields[i].selectedHospital} )';
        row.cells[1].stringFormat.alignment = PdfTextAlignment.center;
        row.cells[1].stringFormat.textDirection = PdfTextDirection.rightToLeft;
        row.cells[0].value = '${fieldsController.fields[i].city}';
        row.cells[0].stringFormat.alignment = PdfTextAlignment.center;
        row.cells[0].stringFormat.textDirection = PdfTextDirection.rightToLeft;
      }
    }

    // Add the grid to a page and save the PDF
    PdfDocument document = PdfDocument();
    document.pageSettings.size = PdfPageSize.a4;
    PdfPage page = document.pages.add();
    grid.draw(
      page: page,
      bounds: Rect.fromLTWH(
        0,
        35,
        page.getClientSize().width,
        page.getClientSize().height,
      ),
    );

    document.pageSettings.size = PdfPageSize.a4;

    PdfGraphics graphics = page.graphics;
    PdfFont font = PdfTrueTypeFont(await _readFontData(), 15);
    graphics.drawString("المرضى المشغولين ", font,
        brush: PdfBrushes.black,
        bounds: Rect.fromLTWH(35, 0, page.getClientSize().width - 200,
            page.getClientSize().height),
        format: PdfStringFormat(
            textDirection: PdfTextDirection.rightToLeft,
            alignment: PdfTextAlignment.right,
            paragraphIndent: 35));
    // Save and launch the PDF file
    List<int> pdfData = await document.save();
    document.dispose();
    saveAndLaunchFile(pdfData, 'MY PATIENTS.pdf');
  }

  Future<List<int>> _readFontData() async {
    final ByteData bytes =
        await rootBundle.load('assets/fonts/Amiri-Regular.ttf');
    return bytes.buffer.asUint8List(bytes.offsetInBytes, bytes.lengthInBytes);
  }
}
