import 'package:flutter/material.dart';
import 'package:flutter_iconly/flutter_iconly.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'OperatedPatients.dart';
import 'about.dart';
import 'new patients.dart';
import 'newRemaind.dart';

class Home extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          width: double.infinity,
          height: double.infinity,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/images/wall.jpg'),
              fit: BoxFit.cover,
              colorFilter: ColorFilter.mode(
                Colors.white.withOpacity(0.5), // set the opacity level
                BlendMode.dstIn, // set the blend mode
              ),
            ),
          ),
          child: Stack(
            alignment: Alignment.center,
            children: [
              Positioned(
                top: 10.h,
                left: Get.width / 3,
                child: CircleAvatar(
                  foregroundImage: AssetImage('assets/images/surgery.jpg'),
                  radius: 50.sp,
                ),
              ),
              Transform.translate(
                offset: Offset(-23.w, -15.h),
                child: Container(
                  width: 90.w,
                  child: Text(
                    'MY PATIENTS',
                    style: TextStyle(
                      fontSize: 19.sp,
                      fontFamily: 'AdventPro',
                      fontWeight: FontWeight.bold,
                      letterSpacing: 0.5.w,
                    ),
                  ),
                ),
              ),
              Transform.translate(
                offset: Offset(20.w, -44.h),
                child: GestureDetector(
                  onTap: () {
                    Get.to(About());
                  },
                  child: Container(
                    width: 50.w,
                    child: Row(
                      children: [
                        Icon(
                          IconlyLight.discovery,
                          size: 22.sp,
                        ),
                        Text(
                          'عن التطبيق ',
                          style: TextStyle(
                            fontSize: 12.sp,
                            fontFamily: 'Exo',
                            fontWeight: FontWeight.bold,
                            letterSpacing: 0.5.w,
                            color: Colors.black,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Positioned(
                top: 40.h,
                left: 5.w,
                child: GestureDetector(
                  onTap: () {
                    Get.to(
                      OperatedPatients(),
                      curve: Curves.fastOutSlowIn,
                    );
                  },
                  child: Container(
                    alignment: Alignment.center,
                    height: 15.h,
                    width: 90.w,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15.sp),
                      image: DecorationImage(
                        image:
                            AssetImage('assets/images/operated patients.jpg'),
                        fit: BoxFit.cover,
                        colorFilter: ColorFilter.mode(
                          Colors.black.withOpacity(0.5),
                          BlendMode.darken,
                        ),
                      ),
                    ),
                    child: Text(
                      'المرضى المشغولين',
                      style: TextStyle(
                        fontSize: 15.sp,
                        fontFamily: 'Exo',
                        fontWeight: FontWeight.bold,
                        letterSpacing: 0.5.w,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
              Positioned(
                top: 59.h,
                left: 5.w,
                child: GestureDetector(
                  onTap: () {
                    Get.to(NewRemaind());
                  },
                  child: Container(
                    alignment: Alignment.center,
                    height: 15.h,
                    width: 90.w,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15.sp),
                      image: DecorationImage(
                        image:
                            AssetImage('assets/images/Appointment Booking.jpg'),
                        fit: BoxFit.cover,
                        colorFilter: ColorFilter.mode(
                          Colors.black.withOpacity(0.65),
                          BlendMode.darken,
                        ),
                      ),
                    ),
                    child: Text(
                      'حجز موعد',
                      style: TextStyle(
                        fontSize: 15.sp,
                        fontFamily: 'Exo',
                        fontWeight: FontWeight.bold,
                        letterSpacing: 0.5.w,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
              Positioned(
                top: 78.h,
                left: 5.w,
                child: GestureDetector(
                  onTap: () {
                    Get.to(new_patients());
                  },
                  child: Container(
                    alignment: Alignment.center,
                    height: 15.h,
                    width: 90.w,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15.sp),
                      image: DecorationImage(
                        image: AssetImage('assets/images/new patients.jpg'),
                        fit: BoxFit.cover,
                        colorFilter: ColorFilter.mode(
                          Colors.black.withOpacity(0.65),
                          BlendMode.darken,
                        ),
                      ),
                    ),
                    child: Text(
                      'مريض جديد',
                      style: TextStyle(
                        fontSize: 15.sp,
                        fontFamily: 'Exo',
                        fontWeight: FontWeight.bold,
                        letterSpacing: 0.5.w,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
