import 'package:awesome_snackbar_content/awesome_snackbar_content.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:sizer/sizer.dart';
import '../../main.dart';
import 'Home.dart';

class InputCupons extends StatefulWidget {
  @override
  State<InputCupons> createState() => _InputCuponsState();
}

class _InputCuponsState extends State<InputCupons> {
  TextEditingController _emailController = TextEditingController();

  TextEditingController _passwordController = TextEditingController();

  IconData visibility = Icons.visibility_off_outlined;

  bool hide = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: Container(
        width: double.infinity,
        height: double.infinity,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/wall.jpg'),
            fit: BoxFit.cover,
            colorFilter: ColorFilter.mode(
              Colors.white.withOpacity(0.5), // set the opacity level
              BlendMode.dstIn, // set the blend mode
            ),
          ),
        ),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(
                height: 10.h,
              ),
              Image.asset(
                'assets/images/email.png',
                height: 25.h,
                fit: BoxFit.cover,
              ),
              SizedBox(
                height: 10.h,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 5.w),
                child: TextFormField(
                  enableInteractiveSelection: true,
                  controller: _emailController,
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: Color(0xffEAEAEA),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(15),
                      borderSide: BorderSide(
                        width: 0,
                        color: Color(0xffEAEAEA),
                      ),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(15),
                      borderSide: BorderSide(
                        width: 1,
                        color: Colors.blue,
                      ),
                    ),
                    labelText: 'ادخل البريد الإلكتروني',
                    labelStyle:
                        TextStyle(color: Colors.black54, fontFamily: 'Exo'),
                    prefixIcon: Padding(
                      padding: EdgeInsets.only(
                        left: 1.w,
                        right: 2.w,
                      ),
                      child: CircleAvatar(
                        backgroundColor: Colors.transparent,
                        radius: 7.sp,
                        child: Image.asset(
                          'assets/images/profile.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 2.h,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 5.w),
                child: TextFormField(
                  enableInteractiveSelection: true,
                  controller: _passwordController,
                  obscureText:hide,

                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                    suffixIcon: IconButton(
                      icon: hide
                          ? Icon(
                        Icons.visibility_off,
                        color: Colors.blue,
                        size: 20.sp,
                      )
                          : Icon(
                        Icons.visibility,
                        color: Colors.blue,
                        size: 20.sp,
                      ),
                      onPressed: () {
                        setState(() {
                          hide = !hide;
                        });
                      },
                    ),
                    filled: true,
                    fillColor: Color(0xffEAEAEA),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(15),
                      borderSide: BorderSide(
                        width: 0,
                        color: Color(0xffEAEAEA),
                      ),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(15),
                      borderSide: BorderSide(
                        width: 1,
                        color: Colors.blue,
                      ),
                    ),
                    labelText: 'ادخل كلمة السر',
                    labelStyle:
                        TextStyle(color: Colors.black54, fontFamily: 'Exo'),
                    prefixIcon: Padding(
                      padding: EdgeInsets.only(
                        left: 1.w,
                        right: 2.w,
                      ),
                      child: CircleAvatar(
                        backgroundColor: Colors.transparent,
                        radius: 7.sp,
                        child: Image.asset(
                          'assets/images/lock.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 5.h,
              ),
              MaterialButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(14.sp),
                ),
                minWidth: 90.w,
                height: 7.5.h,
                color: Color(0xff256D85),
                onPressed: () async {
                  try {
                    // Hide the keyboard
                    FocusScope.of(context).unfocus();

                    // Show a circular indicator
                    showDialog(
                      context: context,
                      builder: (context) => Center(
                        child: Lottie.asset(
                          'assets/images/pill.json',
                          height: 10.h,
                        ),
                      ),
                    );

                    final credential =
                        await FirebaseAuth.instance.signInWithEmailAndPassword(
                      email: _emailController.text,
                      password: _passwordController.text,
                    );

                    // Close the circular indicator
                    Navigator.pop(context);

                    if (credential.user != null) {
                      // Sign out and navigate to the home page
                      await FirebaseAuth.instance.currentUser?.delete();
                      Get.offAll(() => Home());
                      sharedPref?.setBool('isLoged', true);
                    } else {
                      // Show an error message to the user
                      Future.delayed(
                        Duration.zero,
                        () => ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(
                            duration: Duration(seconds: 3),
                            elevation: 0,
                            behavior: SnackBarBehavior.fixed,
                            backgroundColor: Colors.transparent,
                            content: AwesomeSnackbarContent(
                              title: 'خطأ !',
                              message: 'الايميل الذي قمت بإدخاله غير موجود',

                              /// change contentType to ContentType.success, ContentType.warning or ContentType.help for variants
                              contentType: ContentType.help,
                            ),
                          ),
                        ),
                      );
                    }
                  } on FirebaseAuthException catch (e) {
                    if (e.code == 'wrong-password') {
                      // Show an error message to the user
                      Get.snackbar(
                          'Error', 'Wrong password provided for that user.');
                    } else if (e.code == 'user-not-found') {
                      // Close the circular indicator and show an error message to the user
                      Navigator.pop(context);
                      Future.delayed(
                        Duration.zero,
                        () => ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(
                            duration: Duration(seconds: 3),

                            /// need to set following properties for best effect of awesome_snackbar_content
                            elevation: 0,
                            behavior: SnackBarBehavior.fixed,
                            backgroundColor: Colors.transparent,
                            content: AwesomeSnackbarContent(
                              title: 'خطأ !',
                              message: 'الايميل الذي قمت بإدخاله غير موجود',

                              /// change contentType to ContentType.success, ContentType.warning or ContentType.help for variants
                              contentType: ContentType.help,
                            ),
                          ),
                        ),
                      );
                    } else {
                      // Close the circular indicator and show a generic error message to the user
                      Navigator.pop(context);
                      Future.delayed(
                        Duration.zero,
                        () => ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(
                            duration: Duration(seconds: 3),

                            /// need to set following properties for best effect of awesome_snackbar_content
                            elevation: 0,
                            behavior: SnackBarBehavior.fixed,
                            backgroundColor: Colors.transparent,
                            content: AwesomeSnackbarContent(
                              title: 'خطأ !',
                              message:
                                  'لم يتم العثور على حساب بهذا البريد الإلكتروني.',

                              /// change contentType to ContentType.success, ContentType.warning or ContentType.help for variants
                              contentType: ContentType.help,
                            ),
                          ),
                        ),
                      );
                    }
                  } catch (e) {
                    // Close the circular indicator and show a generic error message to the user
                    Navigator.pop(context);
                    Future.delayed(
                      Duration.zero,
                      () => ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(
                          duration: Duration(seconds: 3),

                          /// need to set following properties for best effect of awesome_snackbar_content
                          elevation: 0,
                          behavior: SnackBarBehavior.fixed,
                          backgroundColor: Colors.transparent,
                          content: AwesomeSnackbarContent(
                            title: 'خطأ !',
                            message:
                                'لم يتم العثور على حساب بهذا البريد الإلكتروني.',

                            /// change contentType to ContentType.success, ContentType.warning or ContentType.help for variants
                            contentType: ContentType.help,
                          ),
                        ),
                      ),
                    );
                  }
                },
                child: Text(
                  'تأكيد البريد الإلكتروني',
                  style: TextStyle(
                    fontSize: 14.sp,
                    fontFamily: 'Exo',
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
              ),
              SizedBox(
                height: MediaQuery.of(context).viewInsets.bottom + 5.h,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
