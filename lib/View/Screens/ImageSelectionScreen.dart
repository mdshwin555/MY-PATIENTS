// import 'package:image_picker/image_picker.dart';
// import '../../Controllers/FieldsController.dart';
// import 'pa';
//
//
// class ImageSelectionScreen extends StatefulWidget {
//   final Field field;
//
//   const ImageSelectionScreen({Key? key, required this.field}) : super(key: key);
//
//   @override
//   _ImageSelectionScreenState createState() => _ImageSelectionScreenState();
// }
//
// class _ImageSelectionScreenState extends State<ImageSelectionScreen> {
//   final picker = ImagePicker();
//   List<XFile>? _images;
//
//   Future<void> _selectImages() async {
//     final pickedImages = await picker.pickMultiImage();
//     if (pickedImages != null) {
//       setState(() {
//         _images = pickedImages;
//       });
//     }
//   }
//
//   void _saveImages() async {
//     if (_images != null) {
//       final imagePaths = _images!.map((image) => image.path).toList();
//       final newField = widget.field.copyWith(imagePaths: imagePaths);
//       Get.find<FieldsController>().editField(widget.field, newField);
//     }
//     Navigator.of(context).pop();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text('Select Images'),
//       ),
//       body: Column(
//         children: [
//           if (_images != null)
//             Expanded(
//               child: GridView.builder(
//                 gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
//                   crossAxisCount: 3,
//                   crossAxisSpacing: 4.0,
//                   mainAxisSpacing: 4.0,
//                 ),
//                 itemCount: _images!.length,
//                 itemBuilder: (context, index) {
//                   final image = _images![index];
//                   return Image.file(File(image.path));
//                 },
//               ),
//             )
//           else
//             Expanded(
//               child: Center(
//                 child: Text('No images selected'),
//               ),
//             ),
//           ElevatedButton(
//             onPressed: _selectImages,
//             child: Text('Select Images'),
//           ),
//           ElevatedButton(
//             onPressed: _saveImages,
//             child: Text('Save Images'),
//           ),
//         ],
//       ),
//     );
//   }
// }
