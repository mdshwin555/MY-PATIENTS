import 'package:flutter/material.dart';
import 'package:flutter_iconly/flutter_iconly.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:sizer/sizer.dart';
import '../../Controllers/FieldsController.dart';
import '../../main.dart';
import '../Widgets/CustomField.dart';

class FieldEditorScreen extends StatelessWidget {
  final Field field;

  const FieldEditorScreen({Key? key, required this.field}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String? path = sharedPref?.getString("path");
    final FieldsController fieldsController = Get.find();

    final nameController = TextEditingController(text: field.name);
    final operatineController = TextEditingController(text: field.operatine);
    final selectedOperationKindController =
        TextEditingController(text: field.selectedOperationKind);
    final dateController = TextEditingController(text: field.date);
    final hospitalController = TextEditingController(text: field.hospital);
    final selectedHospitalController =
        TextEditingController(text: field.selectedHospital);
    final cityController = TextEditingController(text: field.city);
    final histopathologyController =
        TextEditingController(text: field.histopathology);
    final phoneController = TextEditingController(text: field.phone);
    final notesController = TextEditingController(text: field.notes);
    TextEditingController _imagesController =
        TextEditingController(text: '${field.images}');
    String selectedOperationKind = selectedOperationKindController.text;
    String selectedHospital = selectedHospitalController.text;
    String selectedCity = cityController.text;
    List<XFile>? images;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xff256D85),
        title: Text(
          'تعديل المريض',
          style: TextStyle(
            fontSize: 11.sp,
            fontFamily: 'Exo',
            fontWeight: FontWeight.bold,
            letterSpacing: 0.1.w,
            color: Colors.white,
          ),
        ),
        leading: IconButton(
          onPressed: () {
            Get.back();
          },
          icon: Icon(IconlyLight.arrowRight2),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'اسم المريض',
                style: TextStyle(
                  fontSize: 11.sp,
                  fontFamily: 'Exo',
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                ),
              ),
              CustomFields(
                label: 'أحمد',
                isDate: false,
                isPhone: false,
                icon: Padding(
                  padding: EdgeInsets.only(
                    right: 1.4.w,
                    left: 1.4.w,
                  ),
                  child: CircleAvatar(
                    backgroundColor: Colors.transparent,
                    radius: 5.sp,
                    child: Image.asset(
                      'assets/images/profile.png',
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                controller: nameController,
                isNotes: false,
                isPhoto: false,
                isTime: false,
                validator: (String? value) {
                  if (value!.isEmpty) {
                    return 'يرجى إدخال الاسم';
                  }
                },
              ),
              Text(
                'العملية',
                style: TextStyle(
                  fontSize: 11.sp,
                  fontFamily: 'Exo',
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                ),
              ),
              Row(
                children: [
                  Expanded(
                    flex: 4,
                    child: CustomFields(
                      isTime: false,
                      label: 'مرارة مرارة',
                      isDate: false,
                      isPhone: false,
                      icon: Padding(
                        padding: EdgeInsets.only(
                          right: 1.5.w,
                          left: 1.5.w,
                        ),
                        child: CircleAvatar(
                          radius: 5.sp,
                          child: Image.asset(
                            'assets/images/surgery-tools.png',
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      controller: operatineController,
                      isNotes: false,
                      isPhoto: false,
                      validator: (String? value) {
                        if (value!.isEmpty) {
                          return 'يرجى إدخال العملية';
                        }
                      },
                    ),
                  ),
                  Expanded(
                    child: Container(
                      height: 8.8.h,
                      margin: EdgeInsets.only(right: 2.w, top: 1.h),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: Color(0xffEAEAEA),
                      ),
                      child: PopupMenuButton(
                        itemBuilder: (BuildContext context) {
                          return <PopupMenuEntry>[
                            PopupMenuItem(
                              child: ListTile(
                                //leading: Icon(Icons.filter_alt),
                                title: Text(
                                  'باردة',
                                  style: TextStyle(
                                    fontSize: 11.sp,
                                    fontFamily: 'Exo',
                                    //fontWeight: FontWeight.bold,
                                    color: Colors.black,
                                  ),
                                ),
                                onTap: () {
                                  selectedOperationKind = 'باردة';
                                  Navigator.pop(context);
                                },
                              ),
                            ),
                            PopupMenuItem(
                              child: ListTile(
                                // leading: Icon(Icons.filter_alt),
                                title: Text(
                                  'إسعافية',
                                  style: TextStyle(
                                    fontSize: 11.sp,
                                    fontFamily: 'Exo',
                                    //fontWeight: FontWeight.bold,
                                    color: Colors.black,
                                  ),
                                ),
                                onTap: () {
                                  selectedOperationKind = 'إسعافية';
                                  Navigator.pop(context);
                                },
                              ),
                            ),
                          ];
                        },
                        icon: Image.asset(
                          'assets/images/filter.png',
                          fit: BoxFit.cover,
                          width: 7.w,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Text(
                'تاريخ العملية',
                style: TextStyle(
                  fontSize: 11.sp,
                  fontFamily: 'Exo',
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                ),
              ),
              CustomFields(
                isTime: false,
                label: '${DateFormat('yyyy/MM/dd').format(DateTime.now())}',
                isDate: true,
                isPhone: false,
                icon: Padding(
                  padding: EdgeInsets.only(
                    right: 1.4.w,
                    left: 1.4.w,
                  ),
                  child: CircleAvatar(
                    backgroundColor: Colors.transparent,
                    radius: 5.sp,
                    child: Image.asset(
                      'assets/images/calendar.png',
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                controller: dateController,
                isNotes: false,
                isPhoto: false,
                validator: (String? value) {
                  if (value!.isEmpty) {
                    return 'يرجى إدخال التاريخ';
                  }
                },
              ),
              Text(
                'المستشفى',
                style: TextStyle(
                  fontSize: 11.sp,
                  fontFamily: 'Exo',
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                ),
              ),
              Row(
                children: [
                  Expanded(
                    flex: 4,
                    child: CustomFields(
                      isTime: false,
                      label: 'مشفى المواساة',
                      isDate: false,
                      isPhone: false,
                      icon: Padding(
                        padding: EdgeInsets.only(
                          right: 1.5.w,
                          left: 1.5.w,
                        ),
                        child: CircleAvatar(
                          radius: 5.sp,
                          child: Image.asset(
                            'assets/images/hospital.png',
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      controller: hospitalController,
                      isNotes: false,
                      isPhoto: false,
                      validator: (String? value) {
                        if (value!.isEmpty) {
                          return 'يرجى إدخال المشفى';
                        }
                      },
                    ),
                  ),
                  Expanded(
                    child: Container(
                      height: 8.8.h,
                      margin: EdgeInsets.only(right: 2.w, top: 1.h),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: Color(0xffEAEAEA),
                      ),
                      child: PopupMenuButton(
                        itemBuilder: (BuildContext context) {
                          return <PopupMenuEntry>[
                            PopupMenuItem(
                              child: ListTile(
                                //leading: Icon(Icons.filter_alt),
                                title: Text(
                                  'عام',
                                  style: TextStyle(
                                    fontSize: 11.sp,
                                    fontFamily: 'Exo',
                                    //fontWeight: FontWeight.bold,
                                    color: Colors.black,
                                  ),
                                ),
                                onTap: () {
                                  selectedHospital = 'عام';
                                  Navigator.pop(context);
                                },
                              ),
                            ),
                            PopupMenuItem(
                              child: ListTile(
                                // leading: Icon(Icons.filter_alt),
                                title: Text(
                                  'خاص',
                                  style: TextStyle(
                                    fontSize: 11.sp,
                                    fontFamily: 'Exo',
                                    //fontWeight: FontWeight.bold,
                                    color: Colors.black,
                                  ),
                                ),
                                onTap: () {
                                  selectedHospital = 'خاص';
                                  Navigator.pop(context);
                                },
                              ),
                            ),
                          ];
                        },
                        icon: Image.asset(
                          'assets/images/filter.png',
                          fit: BoxFit.cover,
                          width: 7.w,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Text(
                'المحافظة',
                style: TextStyle(
                  fontSize: 11.sp,
                  fontFamily: 'Exo',
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                ),
              ),
              StatefulBuilder(
                builder: (BuildContext context,
                    void Function(void Function()) setState) {
                  return Container(
                    height: 7.h,
                    width: 95.w,
                    margin: EdgeInsets.only(
                      top: 1.h,
                    ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      color: Color(0xffEAEAEA),
                    ),
                    child: PopupMenuButton(
                      itemBuilder: (BuildContext context) {
                        return <PopupMenuEntry>[
                          PopupMenuItem(
                            child: ListTile(
                              //leading: Icon(Icons.filter_alt),
                              title: Text(
                                'دمشق',
                                style: TextStyle(
                                  fontSize: 11.sp,
                                  fontFamily: 'Exo',
                                  //fontWeight: FontWeight.bold,
                                  color: Colors.black,
                                ),
                              ),
                              onTap: () {
                                setState(() {
                                  selectedCity = 'دمشق';
                                });
                                Navigator.pop(context);
                              },
                            ),
                          ),
                          PopupMenuItem(
                            child: ListTile(
                              //leading: Icon(Icons.filter_alt),
                              title: Text(
                                'ريف دمشق',
                                style: TextStyle(
                                  fontSize: 11.sp,
                                  fontFamily: 'Exo',
                                  //fontWeight: FontWeight.bold,
                                  color: Colors.black,
                                ),
                              ),
                              onTap: () {
                                setState(() {
                                  selectedCity = 'ريف دمشق';
                                });
                                Navigator.pop(context);
                              },
                            ),
                          ),
                          PopupMenuItem(
                            child: ListTile(
                              //leading: Icon(Icons.filter_alt),
                              title: Text(
                                'حلب',
                                style: TextStyle(
                                  fontSize: 11.sp,
                                  fontFamily: 'Exo',
                                  //fontWeight: FontWeight.bold,
                                  color: Colors.black,
                                ),
                              ),
                              onTap: () {
                                setState(() {
                                  selectedCity = 'حلب';
                                });
                                Navigator.pop(context);
                              },
                            ),
                          ),
                          PopupMenuItem(
                            child: ListTile(
                              //leading: Icon(Icons.filter_alt),
                              title: Text(
                                'حماة',
                                style: TextStyle(
                                  fontSize: 11.sp,
                                  fontFamily: 'Exo',
                                  //fontWeight: FontWeight.bold,
                                  color: Colors.black,
                                ),
                              ),
                              onTap: () {
                                setState(() {
                                  selectedCity = 'حماة';
                                });
                                Navigator.pop(context);
                              },
                            ),
                          ),
                          PopupMenuItem(
                            child: ListTile(
                              // leading: Icon(Icons.filter_alt),
                              title: Text(
                                'حمص',
                                style: TextStyle(
                                  fontSize: 11.sp,
                                  fontFamily: 'Exo',
                                  //fontWeight: FontWeight.bold,
                                  color: Colors.black,
                                ),
                              ),
                              onTap: () {
                                setState(() {
                                  selectedCity = 'حمص';
                                });
                                Navigator.pop(context);
                              },
                            ),
                          ),
                          PopupMenuItem(
                            child: ListTile(
                              // leading: Icon(Icons.filter_alt),
                              title: Text(
                                'الحسكة',
                                style: TextStyle(
                                  fontSize: 11.sp,
                                  fontFamily: 'Exo',
                                  //fontWeight: FontWeight.bold,
                                  color: Colors.black,
                                ),
                              ),
                              onTap: () {
                                setState(() {
                                  selectedCity = 'الحسكة';
                                });
                                Navigator.pop(context);
                              },
                            ),
                          ),
                          PopupMenuItem(
                            child: ListTile(
                              // leading: Icon(Icons.filter_alt),
                              title: Text(
                                'السويداء',
                                style: TextStyle(
                                  fontSize: 11.sp,
                                  fontFamily: 'Exo',
                                  //fontWeight: FontWeight.bold,
                                  color: Colors.black,
                                ),
                              ),
                              onTap: () {
                                setState(() {
                                  selectedCity = 'السويداء';
                                });
                                Navigator.pop(context);
                              },
                            ),
                          ),
                          PopupMenuItem(
                            child: ListTile(
                              // leading: Icon(Icons.filter_alt),
                              title: Text(
                                'درعا',
                                style: TextStyle(
                                  fontSize: 11.sp,
                                  fontFamily: 'Exo',
                                  //fontWeight: FontWeight.bold,
                                  color: Colors.black,
                                ),
                              ),
                              onTap: () {
                                setState(() {
                                  selectedCity = 'درعا';
                                });
                                Navigator.pop(context);
                              },
                            ),
                          ),
                          PopupMenuItem(
                            child: ListTile(
                              // leading: Icon(Icons.filter_alt),
                              title: Text(
                                'دير الزور',
                                style: TextStyle(
                                  fontSize: 11.sp,
                                  fontFamily: 'Exo',
                                  //fontWeight: FontWeight.bold,
                                  color: Colors.black,
                                ),
                              ),
                              onTap: () {
                                setState(() {
                                  selectedCity = 'دير الزور';
                                });
                                Navigator.pop(context);
                              },
                            ),
                          ),
                          PopupMenuItem(
                            child: ListTile(
                              // leading: Icon(Icons.filter_alt),
                              title: Text(
                                'طرطوس',
                                style: TextStyle(
                                  fontSize: 11.sp,
                                  fontFamily: 'Exo',
                                  //fontWeight: FontWeight.bold,
                                  color: Colors.black,
                                ),
                              ),
                              onTap: () {
                                setState(() {
                                  selectedCity = 'طرطوس';
                                });
                                Navigator.pop(context);
                              },
                            ),
                          ),
                          PopupMenuItem(
                            child: ListTile(
                              // leading: Icon(Icons.filter_alt),
                              title: Text(
                                'الرقة',
                                style: TextStyle(
                                  fontSize: 11.sp,
                                  fontFamily: 'Exo',
                                  //fontWeight: FontWeight.bold,
                                  color: Colors.black,
                                ),
                              ),
                              onTap: () {
                                setState(() {
                                  selectedCity = 'الرقة';
                                });
                                Navigator.pop(context);
                              },
                            ),
                          ),
                          PopupMenuItem(
                            child: ListTile(
                              // leading: Icon(Icons.filter_alt),
                              title: Text(
                                'اللاذقية',
                                style: TextStyle(
                                  fontSize: 11.sp,
                                  fontFamily: 'Exo',
                                  //fontWeight: FontWeight.bold,
                                  color: Colors.black,
                                ),
                              ),
                              onTap: () {
                                setState(() {
                                  selectedCity = 'اللاذقية';
                                });
                                Navigator.pop(context);
                              },
                            ),
                          ),
                          PopupMenuItem(
                            child: ListTile(
                              // leading: Icon(Icons.filter_alt),
                              title: Text(
                                'القنيطرة',
                                style: TextStyle(
                                  fontSize: 11.sp,
                                  fontFamily: 'Exo',
                                  //fontWeight: FontWeight.bold,
                                  color: Colors.black,
                                ),
                              ),
                              onTap: () {
                                setState(() {
                                  selectedCity = 'القنيطرة';
                                });
                                Navigator.pop(context);
                              },
                            ),
                          ),
                          PopupMenuItem(
                            child: ListTile(
                              // leading: Icon(Icons.filter_alt),
                              title: Text(
                                'إدلب',
                                style: TextStyle(
                                  fontSize: 11.sp,
                                  fontFamily: 'Exo',
                                  //fontWeight: FontWeight.bold,
                                  color: Colors.black,
                                ),
                              ),
                              onTap: () {
                                setState(() {
                                  selectedCity = 'إدلب';
                                });
                                Navigator.pop(context);
                              },
                            ),
                          ),
                        ];
                      },
                      icon: Row(
                        children: [
                          CircleAvatar(
                            radius: 13.sp,
                            child: Image.asset(
                              'assets/images/google-maps.png',
                              fit: BoxFit.cover,
                            ),
                          ),
                          SizedBox(
                            width: 2.w,
                          ),
                          Text(
                            selectedCity,
                            style: TextStyle(
                              color: selectedCity == 'لم يتم الإختيار'
                                  ? Colors.black54
                                  : Colors.black,
                              fontFamily: 'Exo',
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                },
              ),
              Text(
                'التشريح المرضي',
                style: TextStyle(
                  fontSize: 11.sp,
                  fontFamily: 'Exo',
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                ),
              ),
              CustomFields(
                isTime: false,
                label: 'لا يوجد',
                isDate: false,
                isPhone: false,
                icon: Padding(
                  padding: EdgeInsets.only(
                    right: 1.4.w,
                    left: 1.4.w,
                  ),
                  child: CircleAvatar(
                    radius: 5.sp,
                    child: Image.asset(
                      'assets/images/microscope.png',
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                controller: histopathologyController,
                isNotes: false,
                isPhoto: false,
                validator: (String? value) {
                  if (value!.isEmpty) {
                    return 'يرجى إدخال التشريح المرضي';
                  }
                },
              ),
              Text(
                'هاتف المريض',
                style: TextStyle(
                  fontSize: 11.sp,
                  fontFamily: 'Exo',
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                ),
              ),
              CustomFields(
                isTime: false,
                label: '875 198 938 963+',
                isDate: false,
                isPhone: true,
                icon: Padding(
                  padding: EdgeInsets.only(
                    right: 1.4.w,
                    left: 1.4.w,
                  ),
                  child: CircleAvatar(
                    radius: 5.sp,
                    backgroundColor: Colors.transparent,
                    child: Image.asset(
                      'assets/images/telephone-call.png',
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                controller: phoneController,
                isNotes: false,
                isPhoto: false,
                validator: (String? value) {
                  if (value!.isEmpty) {
                    return 'يرجى إدخال موبايل المريض';
                  }
                },
              ),
              Text(
                'المتابعة و الإختلاطات',
                style: TextStyle(
                  fontSize: 11.sp,
                  fontFamily: 'Exo',
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                ),
              ),
              CustomFields(
                isTime: false,
                label:
                    'نزيف , تأخر في التئام الجروح , تفاعلات سلبية مع التخدير',
                isDate: false,
                isPhone: false,
                icon: Padding(
                  padding: EdgeInsets.only(
                    right: 1.4.w,
                    left: 1.4.w,
                  ),
                  child: CircleAvatar(
                    radius: 7.sp,
                    backgroundColor: Colors.transparent,
                    child: Image.asset(
                      'assets/images/to-do-list.png',
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                controller: notesController,
                isNotes: true,
                isPhoto: false,
                validator: (String? value) {
                  if (value!.isEmpty) {
                    return 'يرجى إدخال المتابعة و الاختلاطات';
                  }
                },
              ),
              Text(
                'صور و تحاليل',
                style: TextStyle(
                  fontSize: 11.sp,
                  fontFamily: 'Exo',
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                ),
              ),
              SizedBox(
                height: 1.h,
              ),
              TextFormField(
                controller: _imagesController,
                keyboardType: TextInputType.none,
                decoration: InputDecoration(
                  filled: true,
                  fillColor: Color(0xffEAEAEA),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15),
                    borderSide: BorderSide(
                      width: 0,
                      color: Color(0xffEAEAEA),
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15),
                    borderSide: BorderSide(
                      width: 1,
                      color: Colors.blue,
                    ),
                  ),
                  hintText: 'اختر صور من الاستوديو',
                  hintStyle:
                      TextStyle(color: Colors.black54, fontFamily: 'Exo'),
                  prefixIcon: Padding(
                    padding: EdgeInsets.only(left: 1.w, right: 2.w),
                    child: GestureDetector(
                      onTap: () async {
                        final List<XFile>? images =
                            await ImagePicker().pickMultiImage();
                        if (images != null) {
                          List<String> imagePaths =
                              images.map((image) => image.path).toList();
                          _imagesController.text = imagePaths.join(', ');
                          print('beforeeeeeeeeeeee${imagePaths.length}');
                          //Get.find<FieldsController>().addImages(imagePaths);
                          print('afterrrrrrrrrrrrr${imagePaths.length}');
                        }
                      },
                      child: CircleAvatar(
                        backgroundColor: Colors.transparent,
                        radius: 7.sp,
                        child: Image.asset(
                          'assets/images/camera.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 4.h,
              ),
              MaterialButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(14.sp),
                ),
                minWidth: 90.w,
                height: 7.5.h,
                color:  Color(0xff256D85),
                onPressed: () {
                  final updatedField = Field(
                    name: nameController.text,
                    operatine: operatineController.text,
                    selectedOperationKind: selectedOperationKind,
                    date: dateController.text,
                    hospital: hospitalController.text,
                    selectedHospital: selectedHospital,
                    city: selectedCity,
                    histopathology: histopathologyController.text,
                    phone: phoneController.text,
                    notes: notesController.text,
                    images: [_imagesController.text],
                  );
                  fieldsController.editField(field, updatedField);
                  Get.back();
                },
                child: Text(
                  'تعديل المريض',
                  style: TextStyle(
                    fontSize: 14.sp,
                    fontFamily: 'Exo',
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
