import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:sizer/sizer.dart';

class CustomFields extends StatelessWidget {
  final String label;
  final Widget? icon;
  final bool isPhone;
  final bool isDate;
  final bool isNotes;
  final bool isPhoto;
  final bool isTime;
  final TextEditingController controller;
  final FormFieldValidator<String>? validator;

  CustomFields({
    required this.label,
    this.icon,
    required this.isPhone,
    required this.isDate,
    required this.isNotes,
    required this.isPhoto,
    required this.isTime,
    required this.controller,
    required this.validator,
  });

  @override
  Widget build(BuildContext context) {
    DateTime? date;

    return Padding(
      padding: EdgeInsets.only(
        top: 1.5.h,
        bottom: 1.h,
      ),
      child: TextFormField(
        enableInteractiveSelection: true,
        style: TextStyle(
          color: Colors.black,
          fontFamily: 'Exo',
        ),
        validator: validator,
        maxLines: isNotes ? 3 : 1,
        controller: controller,
        onTap: () async {
          if (isPhoto) {
            final pickedFile =
                await ImagePicker().getImage(source: ImageSource.gallery);
            if (pickedFile != null) {
              controller.text = pickedFile.path;
            }
          } else if (isDate) {
            date = await showDatePicker(
                context: context,
                lastDate: DateTime(2030),
                firstDate: DateTime(2000),
                initialDate: DateTime.now(),
                cancelText: 'خروج',
                confirmText: 'موافق',
                textDirection: TextDirection.rtl,
               );
            if (date != null) {
              controller.text = date.toString().substring(0, 10);
            }
          } else if (isTime) {
            TimeOfDay? newtime = await showTimePicker(
              context: context,
              initialTime: TimeOfDay.now(),
              cancelText: 'خروج',
              confirmText: 'موافق',

              //textDirection: TextDirection.rtl,
            );
            if (newtime != null) {
              controller.text =
                  '${newtime.hour.toString().padLeft(2, '0')}:${newtime.minute.toString().padLeft(2, '0')}';
            }
          }
        },
        keyboardType: isPhone
            ? TextInputType.number
            : isDate
                ? TextInputType.none
                : isPhoto
                    ? TextInputType.none
                    : isTime
                        ? TextInputType.none
                        : TextInputType.name,
        decoration: InputDecoration(
          filled: true,
          fillColor: Color(0xffEAEAEA),
          hintText: isTime ? label : label,
          hintStyle: TextStyle(color: Colors.black54, fontFamily: 'Exo'),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15),
            borderSide: BorderSide(
              width: 0,
              color: Color(0xffEAEAEA),
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15),
            borderSide: BorderSide(
              width: 1,
              color: Colors.blue,
            ),
          ),
          errorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15),
            borderSide: BorderSide(
              width: 1,
              color: Colors.red,
            ),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15),
            borderSide: BorderSide(
              width: 1,
              color: Colors.red,
            ),
          ),
          prefixIcon: icon,
          errorStyle: TextStyle(
            color: Colors.red,
            fontFamily: 'Exo',
          ),
        ),
      ),
    );
  }
}
