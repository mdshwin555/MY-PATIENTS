import 'dart:convert';
//import 'dart:html';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../main.dart';
import 'Notification.dart';

class RemaindController extends GetxController {
  final remaind = <Remaind>[].obs;

  @override
  void onInit() async {
    super.onInit();
    await _loadFields();
    //Noti.initialize(flutterLocalNotificationsPlugin);
    ever(remaind, (_) {
      update(); // Notify observers that fields list has changed
    });
  }

  _loadFields() async {
    final prefs = await SharedPreferences.getInstance();
    final fieldsJson = prefs.getString('remainds');
    if (fieldsJson != null) {
      final fieldsList = (jsonDecode(fieldsJson) as List)
          .map((e) => Remaind.fromJson(e))
          .toList();
      remaind.addAll(fieldsList);
    }
  }

  _saveFields() async {
    final prefs = await SharedPreferences.getInstance();
    final fieldsJson = jsonEncode(remaind.map((e) => e.toJson()).toList());
    await prefs.setString('remainds', fieldsJson);
  }

  aaddField(Remaind field) {
    remaind.add(field);
    _saveFields(); // Save fields to shared preferences
    update(); // Notify observers that fields list has changed
  }

  deleteField(int index) {
    remaind.removeAt(index);
    _saveFields(); // Save fields to shared preferences
    update(); // Notify observers that fields list has changed
  }

  editField(Remaind oldField, Remaind newField) {
    final index = remaind.indexWhere((field) => field == oldField);
    if (index != -1) {
      remaind[index] = newField;
      _saveFields();
    }
  }
}

class Remaind {
  final String name;
  final String operatine;
  final String date;
  final String hospital;
  final String phone;
  final String time;

  Remaind({
    required this.name,
    required this.operatine,
    required this.date,
    required this.hospital,
    required this.phone,
    required this.time,
  });

  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'operatine': operatine,
      'date': date,
      'hospital': hospital,
      'phone': phone,
      'time': time,
    };
  }

  factory Remaind.fromJson(Map<String, dynamic> json) {
    return Remaind(
      name: json['name'],
      operatine: json['operatine'],
      date: json['date'],
      hospital: json['hospital'],
      phone: json['phone'],
      time: json['time'],
    );
  }
}
