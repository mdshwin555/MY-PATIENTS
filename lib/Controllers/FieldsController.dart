import 'dart:convert';
import 'dart:io';
import 'dart:ui';
//import 'dart:html';
import 'package:get/get.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:syncfusion_flutter_pdf/pdf.dart';


import '../View/Screens/mobile.dart';

class FieldsController extends GetxController {
  final fields = <Field>[].obs;

  @override
  void onInit() async {
    super.onInit();
    await _loadFields();
    ever(fields, (_) {
      update(); // Notify observers that fields list has changed
    });
  }

  _loadFields() async {
    final prefs = await SharedPreferences.getInstance();
    final fieldsJson = prefs.getString('fields');
    if (fieldsJson != null) {
      final fieldsList = (jsonDecode(fieldsJson) as List)
          .map((e) => Field.fromJson(e))
          .toList();
      fields.addAll(fieldsList);
    }
  }

  _saveFields() async {
    final prefs = await SharedPreferences.getInstance();
    final fieldsJson = jsonEncode(fields.map((e) => e.toJson()).toList());
    await prefs.setString('fields', fieldsJson);
  }

  aaddField(Field field) {
    fields.add(field);
    _saveFields(); // Save fields to shared preferences

    update(); // Notify observers that fields list has changed
  }

  deleteField(int index) {
    fields.removeAt(index);
    _saveFields(); // Save fields to shared preferences
    update(); // Notify observers that fields list has changed
  }

  editField(Field oldField, Field newField) {
    final index = fields.indexWhere((field) => field == oldField);
    if (index != -1) {
      fields[index] = newField;
      _saveFields();
    }
  }

  addImages(List<String> images) {
    fields.last.images.addAll(images);
    _saveFields(); // Save fields to shared preferences
    update(); // Notify observers that fields list has changed
  }

}

class Field {
  final String name;
  final String operatine;
  final String selectedOperationKind;
  final String date;
  final String hospital;
  final String selectedHospital;
  final String city;
  final String histopathology;
  final String phone;
  final String notes;
  final List<String> images; // new variable

  Field({
    required this.name,
    required this.operatine,
    required this.selectedOperationKind,
    required this.date,
    required this.hospital,
    required this.selectedHospital,
    required this.city,
    required this.histopathology,
    required this.phone,
    required this.notes,
    required this.images, // initialize new variable
  });

  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'operatine': operatine,
      'selectedOperationKind': selectedOperationKind,
      'date': date,
      'hospital': hospital,
      'selectedHospital': selectedHospital,
      'city': city,
      'histopathology': histopathology,
      'phone': phone,
      'notes': notes,
      'images': images, // add images to JSON map
    };
  }

  factory Field.fromJson(Map<String, dynamic> json) {
    return Field(
      name: json['name'],
      operatine: json['operatine'],
      selectedOperationKind: json['selectedOperationKind'],
      date: json['date'],
      hospital: json['hospital'],
      selectedHospital: json['selectedHospital'],
      city: json['city'],
      histopathology: json['histopathology'],
      phone: json['phone'],
      notes: json['notes'],
      images: List<String>.from(json['images']), // parse images from JSON map
    );
  }
}
