import 'package:get/get.dart';

class MyController extends GetxController {
  Rx<DateTime> _endDate = DateTime.now().obs;

  String get countdown {
    Duration difference = _endDate.value.difference(DateTime.now());
    int months = difference.inDays ~/ 30;
    int days = difference.inDays % 30;
    int hours = difference.inHours % 24;
    int minutes = difference.inMinutes % 60;
    return '$days يوم, $hours ساعة, $minutes دقيقة';
  }

  void startCountdown(DateTime endDate) {
    _endDate.value = endDate;
    update();
    Future.delayed(Duration(seconds: 1), () => _updateCountdown());
  }

  void _updateCountdown() {
    update();
    Future.delayed(Duration(seconds: 1), () => _updateCountdown());
  }

}