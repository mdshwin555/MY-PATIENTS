class FieldData {
  final String name;
  final String operation;
  final String date;
  final String hospital;
  final String notes;

  FieldData({
    required this.name,
    required this.operation,
    required this.date,
    required this.hospital,
    required this.notes,
  });
}
